# IRI-2016

The [**International Reference Ionosphere**](http://irimodel.org/) (**IRI**) is a joined project of the [Committee on Space Research](https://cosparhq.cnes.fr/) (COSPAR) and [The International Union of Radio Science](https://www.ursi.org/homepage.php) (URSI). **IRI** is an empirical model specifying
monthly averages of electron density, ion composition, electron temperature, and ion temperature in the altitude range from 50 km to 1500 km.

## Modified IRI-2016

This is a modified version of the 2016 **IRI** ionospheric model code. It was added the following.

0. Fixed a bug due to which ion densities were modeled incorrectly below 300 km when calling the `IRI_SUB` procedure again (and if you use the `IRI_WEB` procedure to build not an altitude profile, but any other, then the `IRI_SUB` procedure will be called more than once) when using the **Richards-Bilitza-Voglozin-2010** ion density model for altitudes below 300 km (which is the default model). This was due to the fact that in the `SECIPRD` procedure in the **iriflip.for** file the value of the `IMAX` variable was initialized using the `DATA` statement, due to which the initialization occurred only when the program was started (respectively, only when the first call to this procedure). The value of this variable was initialized so that the condition in the `FLXCAL` procedure was met, and when this condition was met, the `DE` and `EV` arrays were filled, and then the `IMAX` value itself was changed so that this condition was no longer met. And when these procedures were called again, the value `IMAX` no longer satisfied the condition, and the arrays `DE` and `EV` were not filled with anything and contained garbage values! Because of this, `NaN` arose in the future, and nothing was calculated correctly, and all ion densities below 300 km were equal to 0! The problem is solved by assignment a value of 0 to the variable `IMAX` at the beginning of the `SECIPRD` procedure: then each time this procedure is called, the variable will be initialized. Explanatory screenshots are attached at the end of this README.

1. Possibility of specifying the directory in which the coefficient files required for the operation of the model are located. This is done by passing two additional arguments to the function/subroutine (meaning any function/subroutine that uses file reading, for example, `IRI_SUB` or `IRI_WEB`):

    1. `path` is a string that represents the directory;
    2. `ilpath` is the length of this string.

    Both arguments are added to the end of the list of passed arguments to each such function/subroutine. In the original version of **IRI-2016** it is made so that all files must be in the directory from which the calling program is launched, which is very inconvenient, since there are 55 files without which **IRI-2016** doesn't work.

2. In the `IRI_SUB` subroutine three elements have been added to the output array `OARR`:

     * `OARR(101)` is absolute value of magnetic induction of the Earth's magnetic field (Gauss) at the point `HEIBEG`;
     * `OARR(102)` is magnetic inclination in degrees at point `HEIBEG`;
     * `OARR(103)` is magnetic declination in degrees at point `HEIBEG`.

     This makes sense if the calculation is carried out at one point, i.e. if `HEIBEG=HEIEND` and `HEISTP=0`.

3. The ability to use all functions in **multi-threaded** mode using [**OpenMP**](https://www.openmp.org/). That is, if you parallelize your code in **C**, **C++** or **Fortran** that uses the **IRI** functions/subroutines (at least `read_ig_rz`, `readapf107`, `IRI_SUB` and `IRI_WEB`), using **OpenMP** directives, then everything will work **correctly** for you. In the original version **IRI-2016** the code was written absolutely non-thread-safe:

     * a lot of `COMMON`-blocks are used;
     * many `DATA`-elements are used;
     * **large amount of `SAVE`d variables are used**;
     * all operations with files (mainly reading various coefficients) are performed with fixed `unit`s.

      Because of all of this, when trying to simultaneously calculate profiles in the same process, but in different threads, failures constantly occurred. In order to avoid failures, [`THREADPRIVATE`](https://www.openmp.org/spec-html/5.0/openmpsu104.html) directives were written for all `COMMON`-blocks, `DATA`-elements and `SAVE`d variables in all functions and subroutines, and all operations with files were placed in the so-called critical sections ([`CRITICAL`](https://www.openmp.org/spec-html/5.0/openmpsu89.html)).

### Testing

The **test** folder contains:

* a C++ program for testing the operation of the library in parallel mode and matching the result with the test program **iritest.for** (Initially testing was carried out with the [Web version](https://kauai.ccmc.gsfc.nasa.gov/instantrun/iri/), but with the next update of the site it began to produce incorrect results that differ from the current version of **[iritest.for](https://irimodel.org/IRI-2016/)** (10/13/2021));
* Python programs for visualizing testing results.

The **config** folder contains all the coefficient files needed for **IRI** to work.

## Модифицированная IRI-2016

Это изменённая версия кода модели ионосферы **IRI** 2016 года. В ней добавлено следующее.

0. Исправлена ошибка, из-за которой при повторном вызове процедуры `IRI_SUB` (а если Вы используете процедуру `IRI_WEB` для построения не высотного профиля, а любого другого, то процедура `IRI_SUB` будет вызываться более одного раза) при использовании модели концентраций ионов для высот ниже 300 км **Ричардса-Билицы-Воглозина-2010** (а именно эта модель используется по умолчанию) неправильно моделировались концентрации ионов ниже 300 км. Это было из-за того, что в процедуре `SECIPRD` в файле **iriflip.for** значение переменной `IMAX` инициализировалось с помощью оператора `DATA`, из-за чего инициализация происходила только при запуске программы (соответственно, только при первом вызове этой процедуры). Значение этой переменной инициализировалось так, чтобы выполнилось условие в процедуре `FLXCAL`, и при выполнении этого условия заполнялись массивы `DE` и `EV`, а затем изменялось и само значение `IMAX` так, чтобы это условие уже не выполнялось. А при повторных вызовах этих процедур значение `IMAX` уже не удовлетворяло условию, и массивы `DE` и `EV` ничем не заполнялись и содержали мусорные значения! Из-за этого в дальнейшем возникали `NaN`, и ничего правильно не считалось, и все концентрации ионов ниже 300 км получались равными 0! Проблема решается добавлением в начало процедуры `SECIPRD` присваивания значения 0 переменной `IMAX`: тогда при каждом вызове этой процедуры переменная будет инициализирована. В конце данного README приложены разъясняющие скриншоты.

1. Возможность указания директории, в которой лежат вспомогательные файлы, требуемые для работы модели. Делается это посредством передачи в качестве двух дополнительных аргументов функции/процедуры (имеется в виду любая функция/процедура, которая использует чтение файлов, например, `IRI_SUB` или `IRI_WEB`):

    1. `path` — строка, которая являет собой директорию;
    2. `ilpath` — длина этой строки.

    Оба аргумента добавлены в конец списка передаваемых аргументов каждой такой функции/процедуры. В оригинальной версии **IRI-2016** сделано так, что все файлы должны лежать в директории, из которой запускается вызывающая программа, что весьма неудобно, так как файлов, без которой **IRI-2016** работать не будет, — 55 штук.

2. В процедуре `IRI_SUB` в выходной массив `OARR` добавлено три элемента:

    * `OARR(101)` — модуль магнитной индукции магнитного поля Земли (Гауссы) в точке `HEIBEG`;
    * `OARR(102)` — магнитное наклонение в градусах в точке `HEIBEG`;
    * `OARR(103)` — магнитное склонение в градусах в точке `HEIBEG`.

    Это имеет смысл, если прозводится расчёт в одной точке, т.е. если `HEIBEG=HEIEND`, а `HEISTP=0`.

3. Возможность использования всех функций во **многопоточном** режиме с помощью [**OpenMP**](https://www.openmp.org/). То есть если Вы распараллеливаете свой код на **C**, **C++** или **Fortran**, который использует функции/процедуры **IRI** (как минимум `read_ig_rz`, `readapf107`, `IRI_SUB` и `IRI_WEB`), с помощью директив **OpenMP**, то у Вас всё будет **корректно** работать. В оригинальной версии **IRI-2016** код написан абсолютно непотокобезопасным:

    * используется множество `COMMON`-блоков;
    * используется множество инициализаций с помощью оператора `DATA`;
    * **используется огромное количество `SAVE`-переменных**;
    * все операции с файлами (в основном чтение различных коэффициентов) производятся при фиксированных "юнитах".

     Из-за этого всего при попытках в одном процессе, но в разных потоках, одновременно рассчитывать профили постоянно происходили сбои. Для того, чтобы сбоев не происходило, для всех `COMMON`-блоков, `DATA`-элементов и `SAVE`-переменных во всех функциях были прописаны директивы [`THREADPRIVATE`](https://www.openmp.org/spec-html/5.0/openmpsu104.html), а все операции с файлами были помещены в т.н. критические секции ([`CRITICAL`](https://www.openmp.org/spec-html/5.0/openmpsu89.html)).

### Тестирование

В папке **test** находятся:

* программа на C++ для тестирования работы библиотеки в параллельном режиме и совпадения результата с тестовой программой **iritest.for** (Изначально проводилось тестирование с [Web-версией](https://kauai.ccmc.gsfc.nasa.gov/instantrun/iri/), но с очередным обновлением сайта она стала выдавать некорректные результаты, отличающиеся от актуальной версии **[iritest.for](https://irimodel.org/IRI-2016/)** (13.10.2021));
* программы на Python для визуализации результатов тестирования.

В папке **config** находятся все файлы коэффициентов, необходимые для работы **IRI**.

## Appendix

![SUBROUTINE SECIPRD](./SECIPRD%20IMAX.png "SUBROUTINE SECIPRD")

![SUBROUTINE FLXCAL](./FLXCAL%20IMAX.png "SUBROUTINE FLXCAL")
