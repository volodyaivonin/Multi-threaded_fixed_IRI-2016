cmake_minimum_required(VERSION 3.14)

project(Test_IRI16Library LANGUAGES CXX Fortran)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(Test_IRI16Library test.cpp)

add_subdirectory(../ CMAKE_CURRENT_BINARY_DIR)
target_link_libraries(Test_IRI16Library IRI_Lib)

find_package(OpenMP)
if(OpenMP_CXX_FOUND)
    target_link_libraries(Test_IRI16Library OpenMP::OpenMP_CXX)
    include_directories(OpenMP_CXX_INCLUDE_DIRS)
endif()
