#include <iostream>
#include <iomanip>
#include <sstream>
#include <cstring>
#include <fstream>
#include <chrono>
#include <vector>
#include <cmath>
#include <filesystem>

#include "omp.h"

extern "C"
{
    void read_ig_rz_(char *s, int &len);
    void readapf107_(char *s, int &len);
    void iri_sub_(int *jf, int *jmag, float *xlat, float *xlon, int *iy, int *mmdd, float *hour,
        float *HEIBEG, float *HEIEND, float *HEISTP, float *outf, float *oar, char *s, int &len);
    void iri_web_(int *jmag, int *jf, float *xlat, float *xlon, int *iy, int *mmdd, int *iut,
        float *hour, float *hxx, float *htec_max, int *ivar, float *vbeg, float *vend, float *vstp,
        float *outf, float *oar, char *s, int &len);
}

std::string dateunit_to_str(unsigned short dateunit)
{
    if (dateunit<10)
        return "0"+std::to_string(dateunit);
    else
        return std::to_string(dateunit);
}

std::string timeunit_to_str(unsigned short timeunit, bool msec=false)
{
    if (!msec)
    {
        if (timeunit<10)
            return "0"+std::to_string(timeunit);
        else
            return std::to_string(timeunit);
    }
    else
    {
        if (timeunit<10)
            return "00"+std::to_string(timeunit);
        else if (timeunit<100)
            return "0"+std::to_string(timeunit);
        else
            return std::to_string(timeunit);
    }
}

void print_console_progress(double progress, double max, std::string symb="#")
{
    int barWidth = 100;//Progressbar width in symbols
    std::cout << "[";
    int pos = barWidth * progress/max;
    for (int i = 0; i < barWidth; ++i)
    {
        if (i < pos)
            std::cout << symb;//The symbols that make up the line
        else if (i == pos)
            std::cout << symb;//Symbol at the end of the line
        else
            std::cout << " ";//If the line ends and there is still space, it is filled with spaces
    }
    std::cout << "] " << progress/max * 100.0 << " % \r";
    std::cout.flush();
}

float calc_mistake_percent(float f, float df, unsigned short resulting_amount_of_iterations)
{
    float mistake = 100.0*df/static_cast<float>(resulting_amount_of_iterations);
    if (f != 0.0)
        mistake /= f;
    else
        mistake /= 0.0001;
    return mistake;
}

int main()
{
    //Initializing Variables:
    std::string path_to_IRIconfig_files = "../config/";//Configuration files path
    char *chpath=const_cast<char*>(path_to_IRIconfig_files.c_str());//Converting a string to a character array
    int lenpath=std::strlen(chpath);//Calculating the length of a character array
    int year = 2023;//Year, UT
    int month = 6;//Month, UT
    int day = 25;//Day, UT
    int mmdd = month*100 + day;
    float dechourUT = 5.0;//Decimal hour, UT
    float DecimHour=dechourUT+25.0;//The decimal hour, 25 is added to make it clear that this is UT(!)
    float latitude = 52.8811, longitude = 103.256;//Coordinates in degrees
    int jf[50];//Flags for configuring model parameters
    //Default values:
    for(int i=0; i<50; i++)
        jf[i] = 1;
    jf[4-1] = 0;  // t=B0table f=other models (f)
    jf[5-1] = 0;  // t=CCIR  f=URSI foF2 model (f)
    jf[6-1] = 0;  // t=DS95+DY85   f=RBV10+TTS03 (f)
    jf[12-1] = 0; // Don't write anything to the "messages.txt" file
    jf[21-1] = 0; // f=ion drift not computed (f)
    jf[23-1] = 0; // t=AEROS/ISIS f=TTS Te with PF10.7 (f)
    jf[28-1] = 0; // f=spread-F not computed (f)
    jf[29-1] = 0; // t=old  f=New Topside options (f)
    jf[30-1] = 0; // t=corr f=NeQuick topside (f)
    jf[33-1] = 0; // f=auroral boundary off (f)
    jf[34-1] = 0; // Don't write anything to the "messages.txt" file
    jf[35-1] = 0; // f=auroral E-storm model off (f)
    jf[39-1] = 0; // t=M3000F2 model f=new hmF2 models (f)
    jf[40-1] = 0; // t=AMTB-model f=Shubin-COSMIC model
    jf[47-1] = 0; // t=CGM computation on f=CGM computation off


    float hmin_km = 0.0, hmax_km = 999.0;//Initial and final heights of the altitude profile, km
    float hstep_km = 1.0;//Height step, km
    unsigned short N = int((hmax_km-hmin_km)/hstep_km) + 1;//Amount of profile points
    float *h_km = new float[N];//Array with heights, km

    std::cout<<"The IRI_SUB tests start here..."<<std::endl;
    //Single threaded mode test:
    float *Ne_m3 = new float[N];//Ionospheric electron density altitude profile array, m⁻³
    float *B_Tl = new float[N];//Geomagnetic field magnetic induction module altitude profile array, T
    float *I_deg = new float[N];//Geomagnetic field magnetic inclination altitude profile array, degrees
    float *D_deg = new float[N];//Geomagnetic field magnetic declination altitude profile array, degrees
    float *Tn_K = new float[N];//Ionospheric neutral temperature altitude profile array, K
    float *Ti_K = new float[N];//Ionospheric ion temperature altitude profile array, K
    float *Te_K = new float[N];//Ionospheric electron temperature altitude profile array, K
    float *N_O = new float[N];//Ionospheric O⁺ ion density altitude profile array, %
    float *N_H = new float[N];//Ionospheric H⁺ ion density altitude profile array, %
    float *N_He = new float[N];//Ionospheric He⁺ ion density altitude profile array, %
    float *N_O2 = new float[N];//Ionospheric O₂⁺ ion density altitude profile array, %
    float *N_NO = new float[N];//Ionospheric NO⁺ ion density altitude profile array, %
    float *N_clust = new float[N];//Ionospheric cluster ion density altitude profile array, %
    float *N_N = new float[N];//Ionospheric N⁺ ion density altitude profile array, %

    std::cout<<"Single threaded mode test..."<<std::endl;
    auto start_single = std::chrono::steady_clock::now();
    read_ig_rz_(chpath, lenpath);//Reading a file with Rz12 and IG12 indexes
    readapf107_(chpath, lenpath);//Reading a file with AP and F10.7 indexes

    float NmF2_m3, hmF2_km,  NmF1_m3, hmF1_km,  NmE_m3, hmE_km, M3000F2;
    float SolarZenithAngle, Dip_deg, Modip_deg, Rz12, IG12, F107_daily, F107_81d_av;
    for (unsigned short i = 0; i < N; i++)
    {
        int jmag = 0;//Geographic coordinates (if it were equal to 1, then geomagnetic)
        h_km[i] = hmin_km + static_cast<float>(i)*hstep_km;
        float heibeg = h_km[i];//Initial height, km
        float heiend = heibeg;//Final height, km
        float heistp = 1.0;//Height step, km
        float outf[20*1000];
        float oar[103];
        iri_sub_(jf, &jmag, &latitude, &longitude, &year, &mmdd, &DecimHour, &heibeg, &heiend, &heistp, outf, oar, chpath, lenpath);
        NmF2_m3 = oar[0]; hmF2_km = oar[1]; NmF1_m3 = oar[2]; hmF1_km = oar[3]; NmE_m3 = oar[4]; hmE_km = oar[5]; M3000F2 = oar[35];
        SolarZenithAngle = oar[22]; Dip_deg = oar[24]; Modip_deg = oar[26];
        Rz12 = oar[32]; IG12 = oar[38]; F107_daily = oar[40]; F107_81d_av = oar[45];
        B_Tl[i] = oar[100]/1.E4; I_deg[i] = oar[101]; D_deg[i] = oar[102];
        Ne_m3[i] = outf[0] > 0 ? outf[0] : 0.0;
        Tn_K[i] = outf[1] > 0 ? outf[1] : 0.0;
        Ti_K[i] = outf[2] > 0 ? outf[2] : 0.0;
        Te_K[i] = outf[3] > 0 ? outf[3] : 0.0;
        N_O[i] = outf[4] > 0 ? outf[4] : 0.0;
        N_H[i] = outf[5] > 0 ? outf[5] : 0.0;
        N_He[i] = outf[6] > 0 ? outf[6] : 0.0;
        N_O2[i] = outf[7] > 0 ? outf[7] : 0.0;
        N_NO[i] = outf[8] > 0 ? outf[8] : 0.0;
        //Only if jf[6-1] = 0:
        N_clust[i] = outf[9] > 0 ? outf[9] : 0.0;
        N_N[i] = outf[10] > 0 ? outf[10] : 0.0;
    }
    std::cout<<"NmF2 = "<<NmF2_m3/1.E6<<" cm^(-3), hmF2 = "<<hmF2_km<<" km"<<std::endl;
    std::cout<<"NmF1 = "<<NmF1_m3/1.E6<<" cm^(-3), hmF1 = "<<hmF1_km<<" km"<<std::endl;
    std::cout<<"NmE = "<<NmE_m3/1.E6<<" cm^(-3), hmE = "<<hmE_km<<" km"<<std::endl;
    std::cout<<"M(3000)F2 = "<<M3000F2<<std::endl;
    std::cout<<"Solar Zenith Angle = "<<SolarZenithAngle<<" degrees"<<std::endl;
    std::cout<<"Dip (Magnetic Inclination) = "<<Dip_deg<<" degrees"<<std::endl;
    std::cout<<"Modip (Modified Dip) = "<<Modip_deg<<" degrees"<<std::endl;
    std::cout<<"Rz12 = "<<Rz12<<std::endl;
    std::cout<<"IG12 = "<<IG12<<std::endl;
    std::cout<<"F10.7 (daily) = "<<F107_daily<<std::endl;
    std::cout<<"F10.7 (81-day average) = "<<F107_81d_av<<std::endl;
    auto end_single = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsed_seconds_single = end_single-start_single;

    std::stringstream output_filename;
    output_filename<<"./test_IRI_SUB_IRI-2016_"<<std::fixed<<std::setprecision(4)<<latitude<<"deg_N_"<<longitude<<"deg_E_"<<dateunit_to_str(day);
    output_filename<<'.'<<dateunit_to_str(month)<<'.'<<std::to_string(year)<<'_'<<std::setprecision(2)<<dechourUT;
    output_filename<<"UT.dat";
    std::ofstream output_file(output_filename.str());
    output_file<<"h(km)"<<'\t'<<"Ne(10^5/cm^3)"<<'\t'<<"B(nTl)"<<'\t'<<"I(deg)"<<'\t'<<"D(deg)"<<'\t'<<"Tn(K)"<<'\t'<<"Ti(K)"<<'\t'<<"Te(K)"<<'\t';
    output_file<<"O+(%*10)"<<'\t'<<"N+(%*10)"<<'\t'<<"H+(%*10)"<<'\t'<<"He+(%*10)"<<'\t'<<"O2+(%*10)"<<'\t'<<"NO+(%*10)"<<'\t'<<"Clust(%*10)"<<std::endl;
    for (unsigned short i = 0; i < N; i++)
    {
        output_file<<std::fixed<<std::setprecision(2)<<h_km[i]<<std::setprecision(6)<<'\t'<<Ne_m3[i]/1.E11<<'\t'<<B_Tl[i]*1.E9<<'\t'<<I_deg[i]<<'\t';
        output_file<<D_deg[i]<<'\t'<<std::setprecision(0)<<Tn_K[i]<<'\t'<<Ti_K[i]<<'\t'<<Te_K[i]<<'\t'<<N_O[i]*10<<'\t'<<N_N[i]*10<<'\t'<<N_H[i]*10<<'\t';
        output_file<<N_He[i]*10<<'\t'<<N_O2[i]*10<<'\t'<<N_NO[i]*10<<'\t'<<N_clust[i]*10<<std::endl;
    }
    std::cout<<"Modeling took "<<elapsed_seconds_single.count()<<" seconds"<<std::endl<<std::endl;

    //Multi-threaded mode test:
    unsigned short n_of_loops = 100, sch = 0;//Number of cycles for calculating the altitude profile and counter
    std::vector<std::vector<float> > threads_Ne_m3, threads_B_Tl, threads_I_deg, threads_D_deg, threads_Tn_K, threads_Ti_K, threads_Te_K;
    std::vector<std::vector<float> > threads_N_O, threads_N_H, threads_N_He, threads_N_O2, threads_N_NO, threads_N_clust, threads_N_N;
    std::cout<<"Multi-threaded test starts here..."<<std::endl;
    auto start_multi = std::chrono::steady_clock::now();
    print_console_progress(sch, n_of_loops);
    std::string thread_file_path = "./Multithreading_IRI_SUB/";
    if (!std::filesystem::exists(std::filesystem::path(thread_file_path)))
        std::filesystem::create_directory(thread_file_path);

    std::vector<float> threads_NmF2_m3, threads_hmF2_km, threads_NmF1_m3, threads_hmF1_km, threads_NmE_m3, threads_hmE_km, threads_M3000F2;
    float dNmF2_m3 = 0.0, dhmF2_km = 0.0, dNmF1_m3 = 0.0, dhmF1_km = 0.0, dNmE_m3 = 0.0, dhmE_km = 0.0, dM3000F2 = 0.0;
    std::stringstream output_filename_pars;
    output_filename_pars<<"./test_IRI_SUB_PARAMETERS_IRI-2016_"<<std::fixed;
    output_filename_pars<<std::setprecision(4)<<latitude<<"deg_N_"<<longitude<<"deg_E_" <<dateunit_to_str(day)<<'.';
    output_filename_pars<<dateunit_to_str(month)<<'.'<<std::to_string(year)<<'_'<<std::setprecision(2)<<dechourUT<<"UT.dat";
    std::ofstream output_file_pars(output_filename_pars.str());
    output_file_pars<<"№_of_thread"<<'\t'<<"NmF2(m^(-3))"<<'\t'<<"hmF2(km)"<<'\t'<<"NmF1(m^(-3))"<<'\t';
    output_file_pars<<"hmF1(km)"<<'\t'<<"NmE(m^(-3))"<<'\t'<<"hmE(km)"<<'\t'<<"M(3000)F2"<<std::endl;

#pragma omp parallel// firstprivate(jf, latitude, longitude, year, mmdd, DecimHour, chpath, lenpath)
{
#pragma omp for schedule(static,1)
    for (unsigned short i = 0; i < n_of_loops; i++)
    {
        std::vector<float> current_Ne_m3, current_B_Tl, current_I_deg, current_D_deg, current_Tn_K, current_Ti_K, current_Te_K;
        std::vector<float> current_N_O, current_N_H, current_N_He, current_N_O2, current_N_NO, current_N_clust, current_N_N;
        float current_NmF2_m3, current_hmF2_km,  current_NmF1_m3, current_hmF1_km,  current_NmE_m3, current_hmE_km, current_M3000F2;
        read_ig_rz_(chpath, lenpath);//Reading a file with Rz12 and IG12 indexes
        readapf107_(chpath, lenpath);//Reading a file with AP and F10.7 indexes
        for (unsigned short j = 0; j < N; j++)
        {
            int jmag = 0;//Geographic coordinates (if it were equal to 1, then geomagnetic)
            float heibeg = h_km[j];//Initial height, km
            float heiend = heibeg;//Final height, km
            float heistp = 1.0;//Height step, km
            float outf[20*1000];
            float oar[103];
            iri_sub_(jf, &jmag, &latitude, &longitude, &year, &mmdd, &DecimHour, &heibeg, &heiend, &heistp, outf, oar, chpath, lenpath);
            current_Ne_m3.push_back(outf[0] > 0 ? outf[0] : 0.0);
            current_B_Tl.push_back(oar[100]/1.E4);
            current_I_deg.push_back(oar[101]);
            current_D_deg.push_back(oar[102]);
            current_Tn_K.push_back(outf[1] > 0 ? outf[1] : 0.0);
            current_Ti_K.push_back(outf[2] > 0 ? outf[2] : 0.0);
            current_Te_K.push_back(outf[3] > 0 ? outf[3] : 0.0);
            current_N_O.push_back(outf[4] > 0 ? outf[4] : 0.0);
            current_N_H.push_back(outf[5] > 0 ? outf[5] : 0.0);
            current_N_He.push_back(outf[6] > 0 ? outf[6] : 0.0);
            current_N_O2.push_back(outf[7] > 0 ? outf[7] : 0.0);
            current_N_NO.push_back(outf[8] > 0 ? outf[8] : 0.0);
            //Only if jf[6-1] = 0:
            current_N_clust.push_back(outf[9] > 0 ? outf[9] : 0.0);
            current_N_N.push_back(outf[10] > 0 ? outf[10] : 0.0);
            current_NmF2_m3 = oar[0]; current_hmF2_km = oar[1]; current_NmF1_m3 = oar[2]; current_hmF1_km = oar[3];
            current_NmE_m3 = oar[4]; current_hmE_km = oar[5]; current_M3000F2 = oar[35];
        }
        std::stringstream output_filename_thread;
        output_filename_thread<<thread_file_path<<omp_get_thread_num()<<'_'<<i<<"_test_IRI_SUB_IRI-2016_"<<std::fixed<<std::setprecision(4);
        output_filename_thread<<latitude<<"deg_N_"<<longitude<<"deg_E_" <<dateunit_to_str(day)<<'.'<<dateunit_to_str(month)<<'.';
        output_filename_thread<<std::to_string(year)<<'_'<<std::setprecision(2)<<dechourUT<<"UT.dat";
        std::ofstream output_file_thread(output_filename_thread.str());
        output_file_thread<<"h(km)"<<'\t'<<"Ne(10^5/cm^3)"<<'\t'<<"B(nTl)"<<'\t'<<"I(deg)"<<'\t'<<"D(deg)"<<'\t'<<"Tn(K)"<<'\t'<<"Ti(K)"<<'\t'<<"Te(K)"<<'\t';
        output_file_thread<<"O+(%*10)"<<'\t'<<"N+(%*10)"<<'\t'<<"H+(%*10)"<<'\t'<<"He+(%*10)"<<'\t'<<"O2+(%*10)"<<'\t'<<"NO+(%*10)"<<'\t'<<"Clust(%*10)"<<std::endl;
        for (unsigned short i = 0; i < N; i++)
        {
            output_file_thread<<std::fixed<<std::setprecision(2)<<h_km[i]<<std::setprecision(6)<<'\t'<<current_Ne_m3[i]/1.E11;
            output_file_thread<<'\t'<<current_B_Tl[i]*1.E9<<'\t'<<current_I_deg[i]<<'\t'<<current_D_deg[i]<<'\t'<<std::setprecision(0)<<current_Tn_K[i]<<'\t';
            output_file_thread<<current_Ti_K[i]<<'\t'<<current_Te_K[i]<<'\t'<<current_N_O[i]*10<<'\t'<<current_N_N[i]*10<<'\t'<<current_N_H[i]*10<<'\t';
            output_file_thread<<current_N_He[i]*10<<'\t'<<current_N_O2[i]*10<<'\t'<<current_N_NO[i]*10<<'\t'<<current_N_clust[i]*10<<std::endl;
        }
        #pragma omp critical (vector)
        {
            threads_Ne_m3.push_back(current_Ne_m3);
            threads_B_Tl.push_back(current_B_Tl);
            threads_I_deg.push_back(current_I_deg);
            threads_D_deg.push_back(current_D_deg);
            threads_Tn_K.push_back(current_Tn_K);
            threads_Ti_K.push_back(current_Ti_K);
            threads_Te_K.push_back(current_Te_K);
            threads_N_O.push_back(current_N_O);
            threads_N_H.push_back(current_N_H);
            threads_N_He.push_back(current_N_He);
            threads_N_O2.push_back(current_N_O2);
            threads_N_NO.push_back(current_N_NO);
            //Only if jf[6-1] = 0:
            threads_N_clust.push_back(current_N_clust);
            threads_N_N.push_back(current_N_N);

            threads_NmF2_m3.push_back(current_NmF2_m3);
            threads_hmF2_km.push_back(current_hmF2_km);
            threads_NmF1_m3.push_back(current_NmF1_m3);
            threads_hmF1_km.push_back(current_hmF1_km);
            threads_NmE_m3.push_back(current_NmE_m3);
            threads_hmE_km.push_back(current_hmE_km);
            threads_M3000F2.push_back(current_M3000F2);
            output_file_pars<<omp_get_thread_num()<<'_'<<i<<std::fixed<<std::setprecision(6)<<'\t'<<threads_NmF2_m3.back()<<'\t'<<threads_hmF2_km.back()<<'\t';
            output_file_pars<<threads_NmF1_m3.back()<<'\t'<<threads_hmF1_km.back()<<'\t'<<threads_NmE_m3.back()<<'\t'<<threads_hmE_km.back()<<'\t';
            output_file_pars<<threads_M3000F2.back()<<std::endl;
            dNmF2_m3 += NmF2_m3 - threads_NmF2_m3.back();
            dhmF2_km += hmF2_km - threads_hmF2_km.back();
            dNmF1_m3 += NmF1_m3 - threads_NmF1_m3.back();
            dhmF1_km += hmF1_km - threads_hmF1_km.back();
            dNmE_m3 += NmE_m3 - threads_NmE_m3.back();
            dhmE_km += hmE_km - threads_hmE_km.back();
            dM3000F2 += M3000F2 - threads_M3000F2.back();
        }
        #pragma omp atomic
        sch++;
        #pragma omp critical (progressbar)
        print_console_progress(sch, n_of_loops);
    }
}
    auto end_multi = std::chrono::steady_clock::now();

    unsigned short resulting_amount_of_iterations = threads_Ne_m3.size();
    float *dNe = new float[N];//Ionospheric electron density altitude profile errors array, %
    float *dB = new float[N];//Geomagnetic field magnetic induction module altitude profile errors array, %
    float *dI = new float[N];//Geomagnetic field magnetic inclination altitude profile errors array, %
    float *dD = new float[N];//Geomagnetic field magnetic declination altitude profile errors array, %
    float *dTn = new float[N];//Ionospheric neutral temperature altitude profile errors array, %
    float *dTi = new float[N];//Ionospheric ion temperature altitude profile errors array, %
    float *dTe = new float[N];//Ionospheric electron temperature altitude profile errors array, %
    float *dN_O = new float[N];//Ionospheric O⁺ ion density altitude profile errors array, %
    float *dN_H = new float[N];//Ionospheric H⁺ ion density altitude profile errors array, %
    float *dN_He = new float[N];//Ionospheric He⁺ ion density altitude profile errors array, %
    float *dN_O2 = new float[N];//Ionospheric O₂⁺ ion density altitude profile errors array, %
    float *dN_NO = new float[N];//Ionospheric NO⁺ ion density altitude profile errors array, %
    float *dN_clust = new float[N];//Ionospheric cluster ion density altitude profile errors array, %
    float *dN_N = new float[N];//Ionospheric N⁺ ion density altitude profile errors array, %
    for (unsigned short i = 0; i < N; i++)
    {
        float dNei_m3 = 0.0, dBi_Tl = 0.0, dIi_deg = 0.0, dDi_deg = 0.0, dTni_K = 0.0, dTii_K = 0.0, dTei_K = 0.0;
        float dN_Oi = 0.0, dN_Hi = 0.0, dN_Hei = 0.0, dN_O2i = 0.0, dN_NOi = 0.0, dN_clusti = 0.0, dN_Ni = 0.0;
        for (unsigned short j = 0; j < resulting_amount_of_iterations; j++)
        {
            dNei_m3 += std::fabs(Ne_m3[i] - threads_Ne_m3[j][i]);
            dBi_Tl += std::fabs(B_Tl[i] - threads_B_Tl[j][i]);
            dIi_deg += std::fabs(I_deg[i] - threads_I_deg[j][i]);
            dDi_deg += std::fabs(D_deg[i] - threads_D_deg[j][i]);

            dTni_K += std::fabs(Tn_K[i] - threads_Tn_K[j][i]);
            dTii_K += std::fabs(Ti_K[i] - threads_Ti_K[j][i]);
            dTei_K += std::fabs(Te_K[i] - threads_Te_K[j][i]);
            dN_Oi += std::fabs(N_O[i] - threads_N_O[j][i]);
            dN_Hi += std::fabs(N_H[i] - threads_N_H[j][i]);
            dN_Hei += std::fabs(N_He[i] - threads_N_He[j][i]);
            dN_O2i += std::fabs(N_O2[i] - threads_N_O2[j][i]);
            dN_NOi += std::fabs(N_NO[i] - threads_N_NO[j][i]);
            //Only if jf[6-1] = 0:
            dN_clusti += std::fabs(N_clust[i] - threads_N_clust[j][i]);
            dN_Ni += std::fabs(N_N[i] - threads_N_N[j][i]);
        }
        dNe[i] = calc_mistake_percent(Ne_m3[i], dNei_m3, resulting_amount_of_iterations);
        dB[i] = calc_mistake_percent(B_Tl[i], dBi_Tl, resulting_amount_of_iterations);
        dI[i] = std::fabs(calc_mistake_percent(I_deg[i], dIi_deg, resulting_amount_of_iterations));
        dD[i] = std::fabs(calc_mistake_percent(D_deg[i], dDi_deg, resulting_amount_of_iterations));
        dTn[i] = calc_mistake_percent(Tn_K[i], dTni_K, resulting_amount_of_iterations);
        dTi[i] = calc_mistake_percent(Ti_K[i], dTii_K, resulting_amount_of_iterations);
        dTe[i] = calc_mistake_percent(Te_K[i], dTei_K, resulting_amount_of_iterations);
        dN_O[i] = calc_mistake_percent(N_O[i], dN_Oi, resulting_amount_of_iterations);
        dN_H[i] = calc_mistake_percent(N_H[i], dN_Hi, resulting_amount_of_iterations);
        dN_He[i] = calc_mistake_percent(N_He[i], dN_Hei, resulting_amount_of_iterations);
        dN_O2[i] = calc_mistake_percent(N_O2[i], dN_O2i, resulting_amount_of_iterations);
        dN_NO[i] = calc_mistake_percent(N_NO[i], dN_NOi, resulting_amount_of_iterations);
        dN_clust[i] = calc_mistake_percent(N_clust[i], dN_clusti, resulting_amount_of_iterations);
        dN_N[i] = calc_mistake_percent(N_N[i], dN_Ni, resulting_amount_of_iterations);
    }
    std::stringstream output_filename2;
    output_filename2<<"./test_Multithreading_IRI_SUB_IRI-2016_"<<std::fixed<<std::setprecision(4)<<latitude<<"deg_N_"<<longitude<<"deg_E_";
    output_filename2<<dateunit_to_str(day)<<'.'<<dateunit_to_str(month)<<'.'<<std::to_string(year)<<'_'<<std::setprecision(2)<<dechourUT;
    output_filename2<<"UT.dat";
    std::ofstream output_file2(output_filename2.str());
    output_file2<<"h(km)"<<'\t'<<"<DNe>(%)"<<'\t'<<"<DB>(%)"<<'\t'<<"<DI>(%)"<<'\t'<<"<DD>(%)"<<'\t'<<"<DTn>(%)"<<'\t'<<"<DTi>(%)"<<'\t'<<"<DTe>(%)"<<'\t';
    output_file2<<"<DO+>(%)"<<'\t'<<"<DN+>(%)"<<'\t'<<"<DH+>(%)"<<'\t'<<"<DHe+>(%)"<<'\t'<<"<DO2+>(%)"<<'\t'<<"<DNO+>(%)"<<'\t'<<"<DClust>(%)"<<std::endl;
    for (unsigned short i = 0; i < N; i++)
    {
        output_file2<<std::fixed<<std::setprecision(2)<<h_km[i]<<std::setprecision(6)<<'\t'<<dNe[i]<<'\t'<<dB[i]<<'\t'<<dI[i]<<'\t';
        output_file2<<dD[i]<<dTn[i]<<'\t'<<dTi[i]<<'\t'<<dTe[i]<<'\t'<<dN_O[i]<<'\t'<<dN_N[i]<<'\t'<<dN_H[i]<<'\t'<<dN_He[i]<<'\t';
        output_file2<<dN_O2[i]<<'\t'<<dN_NO[i]<<'\t'<<dN_clust[i]<<std::endl;
    }

    std::chrono::duration<double> elapsed_seconds_multi = end_multi-start_multi;
    std::cout<<std::endl<<"dNmF2 = "<<dNmF2_m3<<" m^(-3), dhmF2 = "<<dhmF2_km<<" km"<<std::endl;
    std::cout<<"dNmF1 = "<<dNmF1_m3<<" m^(-3), dhmF1 = "<<dhmF1_km<<" km"<<std::endl;
    std::cout<<"dNmE = "<<dNmE_m3<<" m^(-3), dhmE = "<<dhmE_km<<" km"<<std::endl;
    std::cout<<"dM(3000)F2 = "<<dM3000F2<<std::endl;
    std::cout<<"Multithreading modeling took "<<elapsed_seconds_multi.count()<<" seconds for "<<sch<<" iterations of loop";
    std::cout<<std::endl<<std::endl;

    delete [] dNe;
    delete [] dB;
    delete [] dI;
    delete [] dD;
    delete [] dTn;
    delete [] dTi;
    delete [] dTe;
    delete [] dN_O;
    delete [] dN_H;
    delete [] dN_He;
    delete [] dN_O2;
    delete [] dN_NO;
    delete [] dN_clust;
    delete [] dN_N;
    delete [] Ne_m3;
    delete [] B_Tl;
    delete [] I_deg;
    delete [] D_deg;
    delete [] Tn_K;
    delete [] Ti_K;
    delete [] Te_K;
    delete [] N_O;
    delete [] N_H;
    delete [] N_He;
    delete [] N_O2;
    delete [] N_NO;
    delete [] N_clust;
    delete [] N_N;


    std::cout<<"The IRI_WEB tests start here..."<<std::endl;
    //Single threaded mode test:
    Ne_m3 = new float[N];
    Tn_K = new float[N];//Ionospheric neutral temperature altitude profile array, K
    Ti_K = new float[N];//Ionospheric ion temperature altitude profile array, K
    Te_K = new float[N];//Ionospheric electron temperature altitude profile array, K
    N_O = new float[N];//Ionospheric O⁺ ion density altitude profile array, %
    N_H = new float[N];//Ionospheric H⁺ ion density altitude profile array, %
    N_He = new float[N];//Ionospheric He⁺ ion density altitude profile array, %
    N_O2 = new float[N];//Ionospheric O₂⁺ ion density altitude profile array, %
    N_NO = new float[N];//Ionospheric NO⁺ ion density altitude profile array, %
    N_clust = new float[N];//Ionospheric cluster ion density altitude profile array, %
    N_N = new float[N];//Ionospheric N⁺ ion density altitude profile array, %

    int jmag = 0;//Geographic coordinates (if it were equal to 1, then geomagnetic)
    int iut = 1; //1 for given time in UT, 0 for given time in LT
    float hxx = 0.0;//The altitude of the point (km) at which the profile will be calculated (if it isn't altitude profile)
    float htec_max = 2000;//If 0, then TEC is not calculated, otherwise it is the upper limit of the integral in km
    int ivar = 1;//Profile type: 1 - altitudinal, 2 - latitudinal, 3 - longitudinal, 4 - annual (year changes), 5 - monthly (month changes),
        //6 - daily (day changes), 7 - day of year changes, 8 - temporary (decimal hour changes)
    float vbeg = 0.0;//The first point value
    float vend = hmax_km;//The last point value
    float vstp = (vend-vbeg)/static_cast<float>(N-1);//The step size
    float outf[20*1000];
    float oar[100*1000];

    std::cout<<"Single threaded mode test..."<<std::endl;
    auto start_single_IRI_WEB = std::chrono::steady_clock::now();
    read_ig_rz_(chpath, lenpath);//Reading a file with Rz12 and IG12 indexes
    readapf107_(chpath, lenpath);//Reading a file with AP and F10.7 indexes

    iri_web_(&jmag, jf, &latitude, &longitude, &year, &mmdd, &iut, &dechourUT, &hxx, &htec_max, &ivar, &vbeg,
        &vend, &vstp, outf, oar, chpath, lenpath);

    //Data data extraction:
    NmF2_m3 = oar[0]; hmF2_km = oar[1]; NmF1_m3 = oar[2]; hmF1_km = oar[3]; NmE_m3 = oar[4]; hmE_km = oar[5]; M3000F2 = oar[35];
    double TEC_m2 = oar[36];
    for (int i = 0; i < N*20; i += 20)
    {
        Ne_m3[i/20] = (outf[i] > 0 ? outf[i] : 0.0);
        Tn_K[i/20] = outf[i+1] > 0 ? outf[i+1] : 0.0;
        Ti_K[i/20] = outf[i+2] > 0 ? outf[i+2] : 0.0;
        Te_K[i/20] = outf[i+3] > 0 ? outf[i+3] : 0.0;
        N_O[i/20] = outf[i+4] > 0 ? outf[i+4] : 0.0;
        N_H[i/20] = outf[i+5] > 0 ? outf[i+5] : 0.0;
        N_He[i/20] = outf[i+6] > 0 ? outf[i+6] : 0.0;
        N_O2[i/20] = outf[i+7] > 0 ? outf[i+7] : 0.0;
        N_NO[i/20] = outf[i+8] > 0 ? outf[i+8] : 0.0;
    //Only if jf[6-1] = 0:
        N_clust[i/20] = outf[i+9] > 0 ? outf[i+9] : 0.0;
        N_N[i/20] = outf[i+10] > 0 ? outf[i+10] : 0.0;
    }

    std::cout<<"NmF2 = "<<NmF2_m3/1.E6<<" cm^(-3), hmF2 = "<<hmF2_km<<" km"<<std::endl;
    std::cout<<"NmF1 = "<<NmF1_m3/1.E6<<" cm^(-3), hmF1 = "<<hmF1_km<<" km"<<std::endl;
    std::cout<<"NmE = "<<NmE_m3/1.E6<<" cm^(-3), hmE = "<<hmE_km<<" km"<<std::endl;
    std::cout<<"M(3000)F2 = "<<M3000F2<<", TEC = "<<TEC_m2/1.E16<<" TECU"<<std::endl;
    auto end_single_IRI_WEB = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsed_seconds_single_IRI_WEB = end_single_IRI_WEB - start_single_IRI_WEB;

    std::stringstream output_filename_IRI_WEB;
    output_filename_IRI_WEB<<"./test_IRI_WEB_IRI-2016_"<<std::fixed<<std::setprecision(4)<<latitude<<"deg_N_"<<longitude<<"deg_E_";
    output_filename_IRI_WEB<<dateunit_to_str(day) <<'.'<<dateunit_to_str(month)<<'.'<<std::to_string(year)<<'_'<<std::setprecision(2)<<dechourUT;
    output_filename_IRI_WEB<<"UT.dat";
    std::ofstream output_file_IRI_WEB(output_filename_IRI_WEB.str());
    output_file_IRI_WEB<<"h(km)"<<'\t'<<"Ne(10^5/cm^3)"<<'\t'<<"Tn(K)"<<'\t'<<"Ti(K)"<<'\t'<<"Te(K)"<<'\t'<<"O+(%*10)"<<'\t'<<"N+(%*10)"<<'\t'<<"H+(%*10)"<<'\t';
    output_file_IRI_WEB<<"He+(%*10)"<<'\t'<<"O2+(%*10)"<<'\t'<<"NO+(%*10)"<<'\t'<<"Clust(%*10)"<<std::fixed<<std::endl;
    for (unsigned short i = 0; i < N; i++)
    {
        output_file_IRI_WEB<<std::setprecision(2)<<h_km[i]<<std::setprecision(6)<<'\t'<<Ne_m3[i]/1.E11<<'\t'<<std::setprecision(0)<<Tn_K[i]<<'\t';
        output_file_IRI_WEB<<Ti_K[i]<<'\t'<<Te_K[i]<<'\t'<<N_O[i]*10<<'\t'<<N_N[i]*10<<'\t'<<N_H[i]*10<<'\t'<<N_He[i]*10<<'\t'<<N_O2[i]*10<<'\t'<<N_NO[i]*10<<'\t';
        output_file_IRI_WEB<<N_clust[i]*10<<std::endl;
    }
    std::cout<<"Modeling took "<<elapsed_seconds_single_IRI_WEB.count()<<" seconds"<<std::endl<<std::endl;

    //Multi-threaded mode test:
    sch = 0;
    threads_Ne_m3.clear(); threads_Tn_K.clear(); threads_Ti_K.clear(); threads_Te_K.clear(); threads_N_O.clear();
    threads_N_H.clear(); threads_N_He.clear(); threads_N_O2.clear(); threads_N_NO.clear(); threads_N_clust.clear(); threads_N_N.clear();
    std::vector<float> threads_TEC_m2;
    threads_NmF2_m3.clear(); threads_hmF2_km.clear(); threads_NmF1_m3.clear(); threads_hmF1_km.clear();
    threads_NmE_m3.clear(); threads_hmE_km.clear(); threads_M3000F2.clear();
    std::cout<<"Multi-threaded test starts here..."<<std::endl;
    auto start_multi_IRI_WEB = std::chrono::steady_clock::now();
    print_console_progress(sch, n_of_loops);
    std::string thread_file_path_IRI_WEB = "./Multithreading_IRI_WEB/";
    if (!std::filesystem::exists(std::filesystem::path(thread_file_path_IRI_WEB)))
        std::filesystem::create_directory(thread_file_path_IRI_WEB);

    float dTEC_m2 = dNmF2_m3 = dhmF2_km = dNmF1_m3 = dhmF1_km = dNmE_m3 = dhmE_km = dM3000F2 = 0.0;
    std::stringstream output_filename_pars_IRI_WEB;
    output_filename_pars_IRI_WEB<<"./test_IRI_WEB_PARAMETERS_IRI-2016_"<<std::fixed;
    output_filename_pars_IRI_WEB<<std::setprecision(4)<<latitude<<"deg_N_"<<longitude<<"deg_E_" <<dateunit_to_str(day)<<'.';
    output_filename_pars_IRI_WEB<<dateunit_to_str(month)<<'.'<<std::to_string(year)<<'_'<<std::setprecision(2)<<dechourUT<<"UT.dat";
    std::ofstream output_file_pars_IRI_WEB(output_filename_pars_IRI_WEB.str());
    output_file_pars_IRI_WEB<<"№_of_thread"<<'\t'<<"NmF2(m^(-3))"<<'\t'<<"hmF2(km)"<<'\t'<<"NmF1(m^(-3))"<<'\t';
    output_file_pars_IRI_WEB<<"hmF1(km)"<<'\t'<<"NmE(m^(-3))"<<'\t'<<"hmE(km)"<<'\t'<<"M(3000)F2"<<'\t'<<"TEC(TECU)"<<std::endl;

#pragma omp parallel
{
#pragma omp for schedule(static, 1)
    for (unsigned short i = 0; i < n_of_loops; i++)
    {
        std::vector<float> current_Ne_m3, current_B_Tl, current_I_deg, current_D_deg, current_Tn_K, current_Ti_K, current_Te_K;
        std::vector<float> current_N_O, current_N_H, current_N_He, current_N_O2, current_N_NO, current_N_clust, current_N_N;
        read_ig_rz_(chpath, lenpath);//Reading a file with Rz12 and IG12 indexes
        readapf107_(chpath, lenpath);//Reading a file with AP and F10.7 indexes
        float outf_thr[20*1000];
        float oar_thr[103];
        iri_web_(&jmag, jf, &latitude, &longitude, &year, &mmdd, &iut, &dechourUT, &hxx, &htec_max, &ivar, &vbeg,
            &vend, &vstp, outf_thr, oar_thr, chpath, lenpath);
        for (int i = 0; i < N*20; i += 20)
        {
            current_Ne_m3.push_back(outf_thr[i] > 0 ? outf_thr[i] : 0.0);
            current_Tn_K.push_back(outf_thr[i+1] > 0 ? outf_thr[i+1] : 0.0);
            current_Ti_K.push_back(outf_thr[i+2] > 0 ? outf_thr[i+2] : 0.0);
            current_Te_K.push_back(outf_thr[i+3] > 0 ? outf_thr[i+3] : 0.0);
            current_N_O.push_back(outf_thr[i+4] > 0 ? outf_thr[i+4] : 0.0);
            current_N_H.push_back(outf_thr[i+5] > 0 ? outf_thr[i+5] : 0.0);
            current_N_He.push_back(outf_thr[i+6] > 0 ? outf_thr[i+6] : 0.0);
            current_N_O2.push_back(outf_thr[i+7] > 0 ? outf_thr[i+7] : 0.0);
            current_N_NO.push_back(outf_thr[i+8] > 0 ? outf_thr[i+8] : 0.0);
            //Only if jf[6-1] = 0:
            current_N_clust.push_back(outf_thr[i+9] > 0 ? outf_thr[i+9] : 0.0);
            current_N_N.push_back(outf_thr[i+10] > 0 ? outf_thr[i+10] : 0.0);
        }
        std::stringstream output_filename_thread;
        output_filename_thread<<thread_file_path_IRI_WEB<<omp_get_thread_num()<<'_'<<i<<"_test_IRI_WEB_IRI-2016_"<<std::fixed;
        output_filename_thread<<std::setprecision(4)<<latitude<<"deg_N_"<<longitude<<"deg_E_" <<dateunit_to_str(day)<<'.';
        output_filename_thread<<dateunit_to_str(month)<<'.'<<std::to_string(year)<<'_'<<std::setprecision(2)<<dechourUT<<"UT.dat";
        std::ofstream output_file_thread(output_filename_thread.str());
        output_file_thread<<"h(km)"<<'\t'<<"Ne(10^5/cm^3)"<<'\t'<<"Tn(K)"<<'\t'<<"Ti(K)"<<'\t'<<"Te(K)"<<'\t';
        output_file_thread<<"O+(%*10)"<<'\t'<<"N+(%*10)"<<'\t'<<"H+(%*10)"<<'\t'<<"He+(%*10)"<<'\t'<<"O2+(%*10)"<<'\t'<<"NO+(%*10)"<<'\t'<<"Clust(%*10)"<<std::endl;
        for (unsigned short i = 0; i < N; i++)
        {
            output_file_thread<<std::fixed<<std::setprecision(2)<<h_km[i]<<std::setprecision(6)<<'\t'<<current_Ne_m3[i]/1.E11<<'\t'<<std::setprecision(0);
            output_file_thread<<current_Tn_K[i]<<'\t'<<current_Ti_K[i]<<'\t'<<current_Te_K[i]<<'\t'<<current_N_O[i]*10<<'\t'<<current_N_N[i]*10<<'\t';
            output_file_thread<<current_N_H[i]*10<<'\t'<<current_N_He[i]*10<<'\t'<<current_N_O2[i]*10<<'\t'<<current_N_NO[i]*10<<'\t'<<current_N_clust[i]*10<<std::endl;
        }
#pragma omp critical (vector2)
        {
            threads_Ne_m3.push_back(current_Ne_m3);
            threads_Tn_K.push_back(current_Tn_K);
            threads_Ti_K.push_back(current_Ti_K);
            threads_Te_K.push_back(current_Te_K);
            threads_N_O.push_back(current_N_O);
            threads_N_H.push_back(current_N_H);
            threads_N_He.push_back(current_N_He);
            threads_N_O2.push_back(current_N_O2);
            threads_N_NO.push_back(current_N_NO);
            //Only if jf[6-1] = 0:
            threads_N_clust.push_back(current_N_clust);
            threads_N_N.push_back(current_N_N);

            threads_NmF2_m3.push_back(oar_thr[0]);
            threads_hmF2_km.push_back(oar_thr[1]);
            threads_NmF1_m3.push_back(oar_thr[2]);
            threads_hmF1_km.push_back(oar_thr[3]);
            threads_NmE_m3.push_back(oar_thr[4]);
            threads_hmE_km.push_back(oar_thr[5]);
            threads_M3000F2.push_back(oar_thr[35]);
            threads_TEC_m2.push_back(oar_thr[36]);
            output_file_pars_IRI_WEB<<omp_get_thread_num()<<'_'<<i<<std::fixed<<std::setprecision(6)<<'\t'<<threads_NmF2_m3.back()<<'\t'<<threads_hmF2_km.back()<<'\t';
            output_file_pars_IRI_WEB<<threads_NmF1_m3.back()<<'\t'<<threads_hmF1_km.back()<<'\t'<<threads_NmE_m3.back()<<'\t'<<threads_hmE_km.back()<<'\t';
            output_file_pars_IRI_WEB<<threads_M3000F2.back()<<'\t'<<threads_TEC_m2.back()/1.E16<<std::endl;
            dNmF2_m3 += NmF2_m3 - threads_NmF2_m3.back();
            dhmF2_km += hmF2_km - threads_hmF2_km.back();
            dNmF1_m3 += NmF1_m3 - threads_NmF1_m3.back();
            dhmF1_km += hmF1_km - threads_hmF1_km.back();
            dNmE_m3 += NmE_m3 - threads_NmE_m3.back();
            dhmE_km += hmE_km - threads_hmE_km.back();
            dM3000F2 += M3000F2 - threads_M3000F2.back();
            dTEC_m2 += TEC_m2 - threads_TEC_m2.back();
        }
#pragma omp atomic
        sch++;
#pragma omp critical (progressbar2)
            print_console_progress(sch, n_of_loops);
    }
}
    auto end_multi_IRI_WEB = std::chrono::steady_clock::now();
    resulting_amount_of_iterations = threads_Ne_m3.size();
    dNe = new float[N];//Ionospheric electron density altitude profile errors array, %
    dTn = new float[N];//Ionospheric neutral temperature altitude profile errors array, %
    dTi = new float[N];//Ionospheric ion temperature altitude profile errors array, %
    dTe = new float[N];//Ionospheric electron temperature altitude profile errors array, %
    dN_O = new float[N];//Ionospheric O⁺ ion density altitude profile errors array, %
    dN_H = new float[N];//Ionospheric H⁺ ion density altitude profile errors array, %
    dN_He = new float[N];//Ionospheric He⁺ ion density altitude profile errors array, %
    dN_O2 = new float[N];//Ionospheric O₂⁺ ion density altitude profile errors array, %
    dN_NO = new float[N];//Ionospheric NO⁺ ion density altitude profile errors array, %
    dN_clust = new float[N];//Ionospheric cluster ion density altitude profile errors array, %
    dN_N = new float[N];//Ionospheric N⁺ ion density altitude profile errors array, %
    for (unsigned short i = 0; i < N; i++)
    {
        float dNei_m3 = 0.0, dTni_K = 0.0, dTii_K = 0.0, dTei_K = 0.0, dN_Oi = 0.0;
        float dN_Hi = 0.0, dN_Hei = 0.0, dN_O2i = 0.0, dN_NOi = 0.0, dN_clusti = 0.0, dN_Ni = 0.0;
        for (unsigned short j = 0; j < resulting_amount_of_iterations; j++)
        {
            dNei_m3 += std::fabs(Ne_m3[i] - threads_Ne_m3[j][i]);
            dTni_K += std::fabs(Tn_K[i] - threads_Tn_K[j][i]);
            dTii_K += std::fabs(Ti_K[i] - threads_Ti_K[j][i]);
            dTei_K += std::fabs(Te_K[i] - threads_Te_K[j][i]);
            dN_Oi += std::fabs(N_O[i] - threads_N_O[j][i]);
            dN_Hi += std::fabs(N_H[i] - threads_N_H[j][i]);
            dN_Hei += std::fabs(N_He[i] - threads_N_He[j][i]);
            dN_O2i += std::fabs(N_O2[i] - threads_N_O2[j][i]);
            dN_NOi += std::fabs(N_NO[i] - threads_N_NO[j][i]);
            //Only if jf[6-1] = 0:
            dN_clusti += std::fabs(N_clust[i] - threads_N_clust[j][i]);
            dN_Ni += std::fabs(N_N[i] - threads_N_N[j][i]);
        }
        dNe[i] = calc_mistake_percent(Ne_m3[i], dNei_m3, resulting_amount_of_iterations);
        dTn[i] = calc_mistake_percent(Tn_K[i], dTni_K, resulting_amount_of_iterations);
        dTi[i] = calc_mistake_percent(Ti_K[i], dTii_K, resulting_amount_of_iterations);
        dTe[i] = calc_mistake_percent(Te_K[i], dTei_K, resulting_amount_of_iterations);
        dN_O[i] = calc_mistake_percent(N_O[i], dN_Oi, resulting_amount_of_iterations);
        dN_H[i] = calc_mistake_percent(N_H[i], dN_Hi, resulting_amount_of_iterations);
        dN_He[i] = calc_mistake_percent(N_He[i], dN_Hei, resulting_amount_of_iterations);
        dN_O2[i] = calc_mistake_percent(N_O2[i], dN_O2i, resulting_amount_of_iterations);
        dN_NO[i] = calc_mistake_percent(N_NO[i], dN_NOi, resulting_amount_of_iterations);
        dN_clust[i] = calc_mistake_percent(N_clust[i], dN_clusti, resulting_amount_of_iterations);
        dN_N[i] = calc_mistake_percent(N_N[i], dN_Ni, resulting_amount_of_iterations);
    }
    std::cout<<std::endl;

    std::stringstream output_filename3;
    output_filename3<<"test_Multithreading_IRI_WEB_IRI-2016_"<<std::fixed<<std::setprecision(4)<<latitude<<"deg_N_"<<longitude<<"deg_E_";
    output_filename3<<dateunit_to_str(day)<<'.'<<dateunit_to_str(month)<<'.'<<std::to_string(year)<<'_'<<std::setprecision(2)<<dechourUT;
    output_filename3<<"UT.dat";
    std::ofstream output_file3(output_filename3.str());
    output_file3<<"h(km)"<<'\t'<<"<DNe>(%)"<<'\t'<<"<DTn>(%)"<<'\t'<<"<DTi>(%)"<<'\t'<<"<DTe>(%)"<<'\t'<<"<DO+>(%)"<<'\t';
    output_file3<<"<DN+>(%)"<<'\t'<<"<DH+>(%)"<<'\t'<<"<DHe+>(%)"<<'\t'<<"<DO2+>(%)"<<'\t'<<"<DNO+>(%)"<<'\t'<<"<DClust>(%)"<<std::fixed<<std::endl;
    for (unsigned short i = 0; i < N; i++)
    {
        output_file3<<std::setprecision(2)<<h_km[i]<<std::setprecision(6)<<'\t'<<dNe[i]<<'\t'<<dTn[i]<<'\t'<<dTi[i]<<'\t';
        output_file3<<dTe[i]<<'\t'<<dN_O[i]<<'\t'<<dN_N[i]<<'\t'<<dN_H[i]<<'\t'<<dN_He[i]<<'\t'<<dN_O2[i]<<'\t'<<dN_NO[i]<<'\t'<<dN_clust[i]<<std::endl;
    }
    std::chrono::duration<double> elapsed_seconds_multi_IRI_WEB = end_multi_IRI_WEB - start_multi_IRI_WEB;
    std::cout<<"dNmF2 = "<<dNmF2_m3<<" m^(-3), dhmF2 = "<<dhmF2_km<<" km"<<std::endl;
    std::cout<<"dNmF1 = "<<dNmF1_m3<<" m^(-3), dhmF1 = "<<dhmF1_km<<" km"<<std::endl;
    std::cout<<"dNmE = "<<dNmE_m3<<" m^(-3), dhmE = "<<dhmE_km<<" km"<<std::endl;
    std::cout<<"dM(3000)F2 = "<<dM3000F2<<", dTEC = "<<dTEC_m2<<" m^(-2)"<<std::endl;
    std::cout<<"Multithreading modeling took "<<elapsed_seconds_multi_IRI_WEB.count()<<" seconds for "<<sch <<" iterations of loop"<<std::endl<<std::endl;

    delete [] dNe;
    delete [] dTn;
    delete [] dTi;
    delete [] dTe;
    delete [] dN_O;
    delete [] dN_H;
    delete [] dN_He;
    delete [] dN_O2;
    delete [] dN_NO;
    delete [] dN_clust;
    delete [] dN_N;
    delete [] Ne_m3;
    delete [] Tn_K;
    delete [] Ti_K;
    delete [] Te_K;
    delete [] N_O;
    delete [] N_H;
    delete [] N_He;
    delete [] N_O2;
    delete [] N_NO;
    delete [] N_clust;
    delete [] N_N;
    delete [] h_km;

    //Latitude test:
    hxx = 280.0;
    ivar = 2;
    vbeg = -90.0;
    vend = 90.0;
    vstp = 1.0;
    N = int((vend-vbeg)/vstp) + 1;
    Ne_m3 = new float[N];
    Tn_K = new float[N];//Ionospheric neutral temperature latitude profile array, K
    Ti_K = new float[N];//Ionospheric ion temperature latitude profile array, K
    Te_K = new float[N];//Ionospheric electron temperature latitude profile array, K
    N_O = new float[N];//Ionospheric O⁺ ion density latitude profile array, %
    N_H = new float[N];//Ionospheric H⁺ ion density latitude profile array, %
    N_He = new float[N];//Ionospheric He⁺ ion density latitude profile array, %
    N_O2 = new float[N];//Ionospheric O₂⁺ ion density latitude profile array, %
    N_NO = new float[N];//Ionospheric NO⁺ ion density latitude profile array, %
    N_clust = new float[N];//Ionospheric cluster ion density latitude profile array, %
    N_N = new float[N];//Ionospheric N⁺ ion density latitude profile array, %
    latitude = 10.0; longitude = 110.0;//Coordinates in degrees
    month = 4;//Month, UT
    day = 25;//Day, UT
    mmdd = month*100 + day;
    dechourUT = 3.0;//Decimal hour, UT
    DecimHour = dechourUT;
    std::cout<<"Latitude test..."<<std::endl;
    auto start_latitude_IRI_WEB = std::chrono::steady_clock::now();
    read_ig_rz_(chpath, lenpath);//Reading a file with Rz12 and IG12 indexes
    readapf107_(chpath, lenpath);//Reading a file with AP and F10.7 indexes
    iri_web_(&jmag, jf, &latitude, &longitude, &year, &mmdd, &iut, &DecimHour, &hxx, &htec_max, &ivar, &vbeg,
        &vend, &vstp, outf, oar, chpath, lenpath);
    auto end_latitude_IRI_WEB = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsed_seconds_latitude_IRI_WEB = end_latitude_IRI_WEB - start_latitude_IRI_WEB;
    std::cout<<"Modeling took "<<elapsed_seconds_latitude_IRI_WEB.count()<<" seconds"<<std::endl<<std::endl;
    for (int i = 0; i < N*20; i += 20)
    {
        Ne_m3[i/20] = (outf[i] > 0 ? outf[i] : 0.0);
        Tn_K[i/20] = outf[i+1] > 0 ? outf[i+1] : 0.0;
        Ti_K[i/20] = outf[i+2] > 0 ? outf[i+2] : 0.0;
        Te_K[i/20] = outf[i+3] > 0 ? outf[i+3] : 0.0;
        N_O[i/20] = outf[i+4] > 0 ? outf[i+4] : 0.0;
        N_H[i/20] = outf[i+5] > 0 ? outf[i+5] : 0.0;
        N_He[i/20] = outf[i+6] > 0 ? outf[i+6] : 0.0;
        N_O2[i/20] = outf[i+7] > 0 ? outf[i+7] : 0.0;
        N_NO[i/20] = outf[i+8] > 0 ? outf[i+8] : 0.0;
        //Only if jf[6-1] = 0:
        N_clust[i/20] = outf[i+9] > 0 ? outf[i+9] : 0.0;
        N_N[i/20] = outf[i+10] > 0 ? outf[i+10] : 0.0;
    }
    std::stringstream output_filename_latitude_IRI_WEB;
    output_filename_latitude_IRI_WEB<<"./latitude_test_IRI_WEB_IRI-2016_"<<std::fixed<<std::setprecision(3)<<hxx<<"km_"<<std::setprecision(0)<<longitude<<"deg_E_";
    output_filename_latitude_IRI_WEB<<dateunit_to_str(day) <<'.'<<dateunit_to_str(month)<<'.'<<std::to_string(year)<<'_'<<std::setprecision(2)<<dechourUT<<"UT.dat";
    std::ofstream output_file_latitude_IRI_WEB(output_filename_latitude_IRI_WEB.str());
    output_file_latitude_IRI_WEB<<"Latitude(deg)"<<'\t'<<"Ne(10^5/cm^3)"<<'\t'<<"Tn(K)"<<'\t'<<"Ti(K)"<<'\t'<<"Te(K)"<<'\t'<<"O+(%*10)"<<'\t'<<"N+(%*10)";
    output_file_latitude_IRI_WEB<<'\t'<<"H+(%*10)"<<'\t'<<"He+(%*10)"<<'\t'<<"O2+(%*10)"<<'\t'<<"NO+(%*10)"<<'\t'<<"Clust(%*10)"<<std::fixed<<std::endl;
    for (unsigned short i = 0; i < N; i++)
    {
        output_file_latitude_IRI_WEB<<std::setprecision(0)<<vbeg+i*vstp<<std::setprecision(6)<<'\t'<<Ne_m3[i]/1.E11<<'\t'<<std::setprecision(0)<<Tn_K[i]<<'\t';
        output_file_latitude_IRI_WEB<<Ti_K[i]<<'\t'<<Te_K[i]<<'\t'<<N_O[i]*10<<'\t'<<N_N[i]*10<<'\t'<<N_H[i]*10<<'\t'<<N_He[i]*10<<'\t'<<N_O2[i]*10<<'\t';
        output_file_latitude_IRI_WEB<<N_NO[i]*10<<'\t'<<N_clust[i]*10<<std::endl;
    }

    delete [] Ne_m3;
    delete [] Tn_K;
    delete [] Ti_K;
    delete [] Te_K;
    delete [] N_O;
    delete [] N_H;
    delete [] N_He;
    delete [] N_O2;
    delete [] N_NO;
    delete [] N_clust;
    delete [] N_N;

    //Demonstration of speed
    std::cout<<"Demonstration of speed..."<<std::endl;
    jf[22-1] = 0; //ion densities in m-3
    vbeg = 170.0;
    vend = 2000.0;
    vstp = 2.0;
    unsigned short N_h = int((vend-vbeg)/vstp) + 1;
    dechourUT = 0.0;//Decimal hour, U
    float hour_step = 0.0166667; //One minute
    std::vector<float> hours{dechourUT};
    while (hours.back() < 24.0)
        hours.push_back(hours.back()+hour_step);
    if (hours.back() > 24.0)
        hours.pop_back();
    unsigned short N_times = hours.size();

    float **Ne_m3_3D = new float*[N_times];//Ionospheric electron density altitude profile arrays array, m⁻³
    float **Tn_K_3D = new float*[N_times];//Ionospheric neutral temperature altitude profile arrays array, K
    float **Ti_K_3D = new float*[N_times];//Ionospheric ion temperature altitude profile arrays array, K
    float **Te_K_3D = new float*[N_times];//Ionospheric electron temperature altitude profile arrays array, K
    float **N_O_m3_3D = new float*[N_times];//Ionospheric O⁺ ion density altitude profile arrays array, m⁻³
    float **N_H_m3_3D = new float*[N_times];//Ionospheric H⁺ ion density altitude profile arrays array, m⁻³
    float **N_He_m3_3D = new float*[N_times];//Ionospheric He⁺ ion density altitude profile arrays array, m⁻³
    float **N_O2_m3_3D = new float*[N_times];//Ionospheric O₂⁺ ion density altitude profile arrays array, m⁻³
    float **N_NO_m3_3D = new float*[N_times];//Ionospheric NO⁺ ion density altitude profile arrays array, m⁻³
    float **N_clust_m3_3D = new float*[N_times];//Ionospheric cluster ion density altitude profile arrays array, m⁻³
    float **N_N_m3_3D = new float*[N_times];//Ionospheric N⁺ ion density altitude profile arrays array, m⁻³
    for (unsigned short i=0; i<N_times; i++)
    {
        Ne_m3_3D[i] = new float[N_h];
        Tn_K_3D[i] = new float[N_h];
        Ti_K_3D[i] = new float[N_h];
        Te_K_3D[i] = new float[N_h];
        N_O_m3_3D[i] = new float[N_h];
        N_H_m3_3D[i] = new float[N_h];
        N_He_m3_3D[i] = new float[N_h];
        N_O2_m3_3D[i] = new float[N_h];
        N_NO_m3_3D[i] = new float[N_h];
        N_clust_m3_3D[i] = new float[N_h];
        N_N_m3_3D[i] = new float[N_h];
    }

    latitude = 52.8811; longitude = 103.256;//Coordinates in degrees
    year = 2022;
    month = 2;//Month, UT
    day = 1;//Day, UT
    mmdd = month*100 + day;
    hxx = vbeg;
    ivar = 1;

    //Single-threaded mode
    std::cout<<"Single-threaded mode..."<<std::endl;
    auto start_speed_test_single = std::chrono::steady_clock::now();
    read_ig_rz_(chpath, lenpath);//Reading a file with Rz12 and IG12 indexes
    readapf107_(chpath, lenpath);//Reading a file with AP and F10.7 indexes
    for (unsigned short i=0; i<N_times; i++)
    {
        DecimHour = hours[i];
        iri_web_(&jmag, jf, &latitude, &longitude, &year, &mmdd, &iut, &DecimHour, &hxx, &htec_max, &ivar, &vbeg, &vend, &vstp, outf, oar, chpath, lenpath);
        for (int j = 0; j < N_h*20; j += 20)
        {
            Ne_m3_3D[i][j/20] = outf[j] > 0 ? outf[j] : 0.0;
            Tn_K_3D[i][j/20] = outf[j+1] > 0 ? outf[j+1] : 0.0;
            Ti_K_3D[i][j/20] = outf[j+2] > 0 ? outf[j+2] : 0.0;
            Te_K_3D[i][j/20] = outf[j+3] > 0 ? outf[j+3] : 0.0;
            N_O_m3_3D[i][j/20] = outf[j+4] > 0 ? outf[j+4] : 0.0;
            N_H_m3_3D[i][j/20] = outf[j+5] > 0 ? outf[j+5] : 0.0;
            N_He_m3_3D[i][j/20] = outf[j+6] > 0 ? outf[j+6] : 0.0;
            N_O2_m3_3D[i][j/20] = outf[j+7] > 0 ? outf[j+7] : 0.0;
            N_NO_m3_3D[i][j/20] = outf[j+8] > 0 ? outf[j+8] : 0.0;
            //Only if jf[6-1] = 0:
            N_clust_m3_3D[i][j/20] = outf[j+9] > 0 ? outf[j+9] : 0.0;
            N_N_m3_3D[i][j/20] = outf[j+10] > 0 ? outf[j+10] : 0.0;
        }
    }
    auto end_speed_test_single = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsed_seconds_speed_test_single = end_speed_test_single - start_speed_test_single;
    std::cout<<"Modeling took "<<elapsed_seconds_speed_test_single.count()<<" seconds"<<std::endl;
    std::cout<<"Amount of x: "<<N_times<<", amount of y: "<<N_h<<", amount of z: "<<N_times*N_h<<std::endl<<std::endl;
    std::stringstream output_filename_speed_test_single;
    output_filename_speed_test_single<<"./speed_test_IRI_WEB_IRI-2016_single_"<<std::fixed<<std::setprecision(3)<<latitude<<"deg_N_"<<longitude<<"deg_E_";
    output_filename_speed_test_single<<dateunit_to_str(day) <<'.'<<dateunit_to_str(month)<<'.'<<std::to_string(year)<<".dat";
    std::ofstream output_file_speed_test_single(output_filename_speed_test_single.str());
    output_file_speed_test_single<<"Dechour(UT)"<<'\t'<<"h(km)"<<'\t'<<"Ne(10^5/cm^3)"<<'\t'<<"Tn(K)"<<'\t'<<"Ti(K)"<<'\t'<<"Te(K)"<<'\t';
    output_file_speed_test_single<<"O+(10^5/cm^3)"<<'\t'<<"N+(10^5/cm^3)"<<'\t'<<"H+(10^5/cm^3)"<<'\t'<<"He+(10^5/cm^3)"<<'\t'<<"O2+(10^5/cm^3)"<<'\t';
    output_file_speed_test_single<<"NO+(10^5/cm^3)"<<'\t'<<"Clust(10^5/cm^3)"<<std::fixed<<std::setprecision(6)<<std::endl;
    for (unsigned short i=0; i<N_times; i++)
    {
        for (unsigned short j=0; j<N_h; j++)
        {
            output_file_speed_test_single<<hours[i]<<'\t'<<std::setprecision(2)<<vbeg+j*vstp<<std::setprecision(6)<<'\t';
            output_file_speed_test_single<<Ne_m3_3D[i][j]/1.E11<<'\t'<<Tn_K_3D[i][j]<<'\t'<<Ti_K_3D[i][j]<<'\t'<<Te_K_3D[i][j]<<'\t';
            output_file_speed_test_single<<N_O_m3_3D[i][j]/1.E11<<'\t'<<N_N_m3_3D[i][j]/1.E11<<'\t'<<N_H_m3_3D[i][j]/1.E11<<'\t'<<N_He_m3_3D[i][j]/1.E11<<'\t';
            output_file_speed_test_single<<N_O2_m3_3D[i][j]/1.E11<<'\t'<<N_NO_m3_3D[i][j]/1.E11<<'\t'<<N_clust_m3_3D[i][j]/1.E11<<std::endl;
        }
        output_file_speed_test_single<<std::endl;
    }

    for (unsigned short i=0; i<N_times; i++)
    {
        delete [] Ne_m3_3D[i];
        delete [] Tn_K_3D[i];
        delete [] Ti_K_3D[i];
        delete [] Te_K_3D[i];
        delete [] N_O_m3_3D[i];
        delete [] N_H_m3_3D[i];
        delete [] N_He_m3_3D[i];
        delete [] N_O2_m3_3D[i];
        delete [] N_NO_m3_3D[i];
        delete [] N_clust_m3_3D[i];
        delete [] N_N_m3_3D[i];
    }
    delete [] Ne_m3_3D;
    delete [] Tn_K_3D;
    delete [] Ti_K_3D;
    delete [] Te_K_3D;
    delete [] N_O_m3_3D;
    delete [] N_H_m3_3D;
    delete [] N_He_m3_3D;
    delete [] N_O2_m3_3D;
    delete [] N_NO_m3_3D;
    delete [] N_clust_m3_3D;
    delete [] N_N_m3_3D;

    //Multi-threaded mode
    std::cout<<"Multi-threaded mode..."<<std::endl;
    omp_set_dynamic(0);// Explicitly disable dynamic teams
    unsigned short max_threads_numbers = 32;
    std::ofstream time_file("Speed_demonstration.dat");
    time_file<<"Threads_number"<<'\t'<<"Execution_time(s)"<<std::fixed<<std::endl;
    for (unsigned short thread_num = 1; thread_num <= max_threads_numbers; thread_num++)
    {
        Ne_m3_3D = new float*[N_times];//Ionospheric electron density altitude profile arrays array, m⁻³
        Tn_K_3D = new float*[N_times];//Ionospheric neutral temperature altitude profile arrays array, K
        Ti_K_3D = new float*[N_times];//Ionospheric ion temperature altitude profile arrays array, K
        Te_K_3D = new float*[N_times];//Ionospheric electron temperature altitude profile arrays array, K
        N_O_m3_3D = new float*[N_times];//Ionospheric O⁺ ion density altitude profile arrays array, m⁻³
        N_H_m3_3D = new float*[N_times];//Ionospheric H⁺ ion density altitude profile arrays array, m⁻³
        N_He_m3_3D = new float*[N_times];//Ionospheric He⁺ ion density altitude profile arrays array, m⁻³
        N_O2_m3_3D = new float*[N_times];//Ionospheric O₂⁺ ion density altitude profile arrays array, m⁻³
        N_NO_m3_3D = new float*[N_times];//Ionospheric NO⁺ ion density altitude profile arrays array, m⁻³
        N_clust_m3_3D = new float*[N_times];//Ionospheric cluster ion density altitude profile arrays array, m⁻³
        N_N_m3_3D = new float*[N_times];//Ionospheric N⁺ ion density altitude profile arrays array, m⁻³
        for (unsigned short i=0; i<N_times; i++)
        {
            Ne_m3_3D[i] = new float[N_h];
            Tn_K_3D[i] = new float[N_h];
            Ti_K_3D[i] = new float[N_h];
            Te_K_3D[i] = new float[N_h];
            N_O_m3_3D[i] = new float[N_h];
            N_H_m3_3D[i] = new float[N_h];
            N_He_m3_3D[i] = new float[N_h];
            N_O2_m3_3D[i] = new float[N_h];
            N_NO_m3_3D[i] = new float[N_h];
            N_clust_m3_3D[i] = new float[N_h];
            N_N_m3_3D[i] = new float[N_h];
        }

        omp_set_num_threads(thread_num);// Use thread_num threads for all consecutive parallel regions
        // std::cout<<"Multi-threaded mode..."<<std::endl;
        auto start_speed_test_multi = std::chrono::steady_clock::now();
        unsigned short get_num_threads = 0;
        std::cout<<"Testing with "<<thread_num<<" threads...";
    #pragma omp parallel //num_threads(1)
    {
        get_num_threads = omp_get_num_threads();
        read_ig_rz_(chpath, lenpath);//Reading a file with Rz12 and IG12 indexes
        readapf107_(chpath, lenpath);//Reading a file with AP and F10.7 indexes
        float outf_thr[20*1000];
        float oar_thr[103];
    #pragma omp for schedule(static, 1)
        for (unsigned short i=0; i<N_times; i++)
        {
            DecimHour = hours[i];
            iri_web_(&jmag, jf, &latitude, &longitude, &year, &mmdd, &iut, &DecimHour, &hxx, &htec_max, &ivar, &vbeg, &vend, &vstp,
                outf_thr, oar_thr, chpath, lenpath);
            for (int j = 0; j < N_h*20; j += 20)
            {
                Ne_m3_3D[i][j/20] = outf_thr[j] > 0 ? outf_thr[j] : 0.0;
                Tn_K_3D[i][j/20] = outf_thr[j+1] > 0 ? outf_thr[j+1] : 0.0;
                Ti_K_3D[i][j/20] = outf_thr[j+2] > 0 ? outf_thr[j+2] : 0.0;
                Te_K_3D[i][j/20] = outf_thr[j+3] > 0 ? outf_thr[j+3] : 0.0;
                N_O_m3_3D[i][j/20] = outf_thr[j+4] > 0 ? outf_thr[j+4] : 0.0;
                N_H_m3_3D[i][j/20] = outf_thr[j+5] > 0 ? outf_thr[j+5] : 0.0;
                N_He_m3_3D[i][j/20] = outf_thr[j+6] > 0 ? outf_thr[j+6] : 0.0;
                N_O2_m3_3D[i][j/20] = outf_thr[j+7] > 0 ? outf_thr[j+7] : 0.0;
                N_NO_m3_3D[i][j/20] = outf_thr[j+8] > 0 ? outf_thr[j+8] : 0.0;
                //Only if jf[6-1] = 0:
                N_clust_m3_3D[i][j/20] = outf_thr[j+9] > 0 ? outf_thr[j+9] : 0.0;
                N_N_m3_3D[i][j/20] = outf_thr[j+10] > 0 ? outf_thr[j+10] : 0.0;
            }
        }
    }
        auto end_speed_test_multi = std::chrono::steady_clock::now();
        std::chrono::duration<double> elapsed_seconds_speed_test_multi = end_speed_test_multi - start_speed_test_multi;
        std::cout<<"Modeling took "<<elapsed_seconds_speed_test_multi.count()<<" seconds with "<<get_num_threads<<" threads"<<std::endl;//<<std::endl;
        std::stringstream output_filename_speed_test_multi;
        output_filename_speed_test_multi<<"./speed_test_IRI_WEB_IRI-2016_multi_"<<std::fixed<<std::setprecision(3)<<latitude<<"deg_N_"<<longitude<<"deg_E_";
        output_filename_speed_test_multi<<dateunit_to_str(day) <<'.'<<dateunit_to_str(month)<<'.'<<std::to_string(year)<<".dat";
        std::ofstream output_file_speed_test_multi(output_filename_speed_test_multi.str());
        output_file_speed_test_multi<<"Dechour(UT)"<<'\t'<<"h(km)"<<'\t'<<"Ne(10^5/cm^3)"<<'\t'<<"Tn(K)"<<'\t'<<"Ti(K)"<<'\t'<<"Te(K)"<<'\t';
        output_file_speed_test_multi<<"O+(10^5/cm^3)"<<'\t'<<"N+(10^5/cm^3)"<<'\t'<<"H+(10^5/cm^3)"<<'\t'<<"He+(10^5/cm^3)"<<'\t'<<"O2+(10^5/cm^3)"<<'\t';
        output_file_speed_test_multi<<"NO+(10^5/cm^3)"<<'\t'<<"Clust(10^5/cm^3)"<<std::fixed<<std::setprecision(6)<<std::endl;
        for (unsigned short i=0; i<N_times; i++)
        {
            for (unsigned short j=0; j<N_h; j++)
            {
                output_file_speed_test_multi<<hours[i]<<'\t'<<std::setprecision(2)<<vbeg+j*vstp<<std::setprecision(6)<<'\t';
                output_file_speed_test_multi<<Ne_m3_3D[i][j]/1.E11<<'\t'<<Tn_K_3D[i][j]<<'\t'<<Ti_K_3D[i][j]<<'\t'<<Te_K_3D[i][j]<<'\t';
                output_file_speed_test_multi<<N_O_m3_3D[i][j]/1.E11<<'\t'<<N_N_m3_3D[i][j]/1.E11<<'\t'<<N_H_m3_3D[i][j]/1.E11<<'\t'<<N_He_m3_3D[i][j]/1.E11<<'\t';
                output_file_speed_test_multi<<N_O2_m3_3D[i][j]/1.E11<<'\t'<<N_NO_m3_3D[i][j]/1.E11<<'\t'<<N_clust_m3_3D[i][j]/1.E11<<std::endl;
            }
            output_file_speed_test_multi<<std::endl;
        }

        for (unsigned short i=0; i<N_times; i++)
        {
            delete [] Ne_m3_3D[i];
            delete [] Tn_K_3D[i];
            delete [] Ti_K_3D[i];
            delete [] Te_K_3D[i];
            delete [] N_O_m3_3D[i];
            delete [] N_H_m3_3D[i];
            delete [] N_He_m3_3D[i];
            delete [] N_O2_m3_3D[i];
            delete [] N_NO_m3_3D[i];
            delete [] N_clust_m3_3D[i];
            delete [] N_N_m3_3D[i];
        }
        delete [] Ne_m3_3D;
        delete [] Tn_K_3D;
        delete [] Ti_K_3D;
        delete [] Te_K_3D;
        delete [] N_O_m3_3D;
        delete [] N_H_m3_3D;
        delete [] N_He_m3_3D;
        delete [] N_O2_m3_3D;
        delete [] N_NO_m3_3D;
        delete [] N_clust_m3_3D;
        delete [] N_N_m3_3D;

        time_file<<std::setprecision(0)<<get_num_threads<<'\t'<<std::setprecision(2)<<elapsed_seconds_speed_test_multi.count()<<std::endl;
    }

    return 0;
}
