from pathlib import Path
import re
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

IRI_SUB_output_dir = './Pictures IRI_SUB/' #IRI_SUB output files directory
IRI_SUB_Ne_output_dir = IRI_SUB_output_dir + 'Ne/' #IRI_SUB Ne output files directory
IRI_SUB_Ni_output_dir = IRI_SUB_output_dir + 'Ni/' #IRI_SUB Ni output files directory
IRI_SUB_T_output_dir = IRI_SUB_output_dir + 'T/' #IRI_SUB T output files directory
IRI_SUB_dats_path = Path("./Multithreading_IRI_SUB") #IRI_SUB dat-files directory
IRI_WEB_output_dir = './Pictures IRI_WEB/' #IRI_WEB output files directory
IRI_WEB_Ne_output_dir = IRI_WEB_output_dir + 'Ne/' #IRI_WEB Ne output files directory
IRI_WEB_Ni_output_dir = IRI_WEB_output_dir + 'Ni/' #IRI_WEB Ni output files directory
IRI_WEB_T_output_dir = IRI_WEB_output_dir + 'T/' #IRI_WEB T output files directory
IRI_WEB_dats_path = Path("./Multithreading_IRI_WEB") #IRI_WEB dat-files directory

if not Path(IRI_SUB_output_dir).is_dir():
    Path(IRI_SUB_output_dir).mkdir()
if not Path(IRI_SUB_Ne_output_dir).is_dir():
    Path(IRI_SUB_Ne_output_dir).mkdir()
if not Path(IRI_SUB_Ni_output_dir).is_dir():
    Path(IRI_SUB_Ni_output_dir).mkdir()
if not Path(IRI_SUB_T_output_dir).is_dir():
    Path(IRI_SUB_T_output_dir).mkdir()
if not Path(IRI_WEB_output_dir).is_dir():
    Path(IRI_WEB_output_dir).mkdir()
if not Path(IRI_WEB_Ne_output_dir).is_dir():
    Path(IRI_WEB_Ne_output_dir).mkdir()
if not Path(IRI_WEB_Ni_output_dir).is_dir():
    Path(IRI_WEB_Ni_output_dir).mkdir()
if not Path(IRI_WEB_T_output_dir).is_dir():
    Path(IRI_WEB_T_output_dir).mkdir()


IRI_SUB_single_threaded_filename = './test_IRI_SUB_IRI-2016_52.8811deg_N_103.2560deg_E_25.06.2023_5.00UT.dat'
IRI_SUB_pars_filename = './test_IRI_SUB_PARAMETERS_IRI-2016_52.8811deg_N_103.2560deg_E_25.06.2023_5.00UT.dat'
IRI_WEB_single_threaded_filename = './test_IRI_WEB_IRI-2016_52.8811deg_N_103.2560deg_E_25.06.2023_5.00UT.dat'
IRI_WEB_pars_filename = './test_IRI_WEB_PARAMETERS_IRI-2016_52.8811deg_N_103.2560deg_E_25.06.2023_5.00UT.dat'
# Web_IRI_filename = 'Web IRI-2016 25.06.2023 05.00 52.8811 N 103.256 E.dat'
Web_IRI_filename = 'IRI-2016 from iritest 25.06.2023 05.00 52.8811 N 103.256 E.7'

matplotlib.use('Agg') #Don't show plots
cm = 1/2.54
plt.rcParams["font.family"] = "Times New Roman"
plt.rcParams["font.size"] = 14

WEB_h_km = []
WEB_Ne_cm3 = []
WEB_Tn_K = []
WEB_Ti_K = []
WEB_Te_K = []
WEB_O = []
WEB_N = []
WEB_H = []
WEB_He = []
WEB_O2 = []
WEB_NO = []
WEB_Clust = []
with open(Web_IRI_filename, 'r') as f:
    prev_line = ''
    table_origin = False
    sch = 0
    for line in f: #File reading
        if (not table_origin):
            if (prev_line == '-\n'):
                table_origin = True
        if line != '' and line != ' ' and table_origin:
            if sch >= 2:
                s0 = re.split('\s+|\n', line)
                s1 = s0[1:] if s0[0]=='' else s0
                WEB_h_km.append(float(s1[0]))
                Ne = float(s1[1])
                WEB_Ne_cm3.append(Ne/1.E5 if Ne >= 0 else 0.0)
                WEB_Tn_K.append(float(s1[3]) if float(s1[3]) >= 0 else 0.0)
                WEB_Ti_K.append(float(s1[4]) if float(s1[4]) >= 0 else 0.0)
                WEB_Te_K.append(float(s1[5]) if float(s1[5]) >= 0 else 0.0)
                WEB_O.append(float(s1[6]) if float(s1[6]) >= 0 else 0.0)
                WEB_N.append(float(s1[7]) if float(s1[7]) >= 0 else 0.0)
                WEB_H.append(float(s1[8]) if float(s1[8]) >= 0 else 0.0)
                WEB_He.append(float(s1[9]) if float(s1[9]) >= 0 else 0.0)
                WEB_O2.append(float(s1[10]) if float(s1[10]) >= 0 else 0.0)
                WEB_NO.append(float(s1[11]) if float(s1[11]) >= 0 else 0.0)
                WEB_Clust.append(float(s1[12]) if float(s1[12]) >= 0 else 0.0)
            sch += 1
        prev_line = line

IRI_SUB_h_km = []
IRI_SUB_Ne_cm3 = []
B_nTl = []
I_deg = []
D_deg = []
IRI_SUB_Tn_K = []
IRI_SUB_Ti_K = []
IRI_SUB_Te_K = []
IRI_SUB_N_O = []
IRI_SUB_N_N = []
IRI_SUB_N_H = []
IRI_SUB_N_He = []
IRI_SUB_N_O2 = []
IRI_SUB_N_NO = []
IRI_SUB_N_Clust = []
with open(IRI_SUB_single_threaded_filename, 'r') as f:
     for i, line in enumerate(f): #File reading
        if line != '\n':
            if i != 0:
                s1 = line.split('\t')
                IRI_SUB_h_km.append(float(s1[0]))
                IRI_SUB_Ne_cm3.append(float(s1[1]))
                B_nTl.append(float(s1[2]))
                I_deg.append(float(s1[3]))
                D_deg.append(float(s1[4]))
                IRI_SUB_Tn_K.append(float(s1[5]))
                IRI_SUB_Ti_K.append(float(s1[6]))
                IRI_SUB_Te_K.append(float(s1[7]))
                IRI_SUB_N_O.append(float(s1[8]))
                IRI_SUB_N_N.append(float(s1[9]))
                IRI_SUB_N_H.append(float(s1[10]))
                IRI_SUB_N_He.append(float(s1[11]))
                IRI_SUB_N_O2.append(float(s1[12]))
                IRI_SUB_N_NO.append(float(s1[13]))
                IRI_SUB_N_Clust.append(float(s1[14]))

IRI_WEB_h_km = []
IRI_WEB_Ne_cm3 = []
IRI_WEB_Tn_K = []
IRI_WEB_Ti_K = []
IRI_WEB_Te_K = []
IRI_WEB_N_O = []
IRI_WEB_N_N = []
IRI_WEB_N_H = []
IRI_WEB_N_He = []
IRI_WEB_N_O2 = []
IRI_WEB_N_NO = []
IRI_WEB_N_Clust = []
with open(IRI_WEB_single_threaded_filename, 'r') as f:
     for i, line in enumerate(f): #File reading
        if line != '\n':
            if i != 0:
                s1 = line.split('\t')
                IRI_WEB_h_km.append(float(s1[0]))
                IRI_WEB_Ne_cm3.append(float(s1[1]))
                IRI_WEB_Tn_K.append(float(s1[2]))
                IRI_WEB_Ti_K.append(float(s1[3]))
                IRI_WEB_Te_K.append(float(s1[4]))
                IRI_WEB_N_O.append(float(s1[5]))
                IRI_WEB_N_N.append(float(s1[6]))
                IRI_WEB_N_H.append(float(s1[7]))
                IRI_WEB_N_He.append(float(s1[8]))
                IRI_WEB_N_O2.append(float(s1[9]))
                IRI_WEB_N_NO.append(float(s1[10]))
                IRI_WEB_N_Clust.append(float(s1[11]))

fig, ax = plt.subplots(1, 1, figsize=(30*cm, 25*cm))
fig.tight_layout()
plt.suptitle(t = 'Comparison 25.06.2023 05:00 UT 52.8811$\\degree$ N 103.256$\\degree$ E',
    fontsize=17, fontweight='bold', y=1.02)
# ax.plot(WEB_Ne_cm3, WEB_h_km, lw=3, label='Web version of IRI-2016')
ax.plot(WEB_Ne_cm3, WEB_h_km, lw=3, label='Version of IRI-2016 from $\\bf iritest.for$')
ax.plot(IRI_SUB_Ne_cm3, IRI_SUB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_SUB")', ls='--', c='g')
ax.plot(IRI_WEB_Ne_cm3, IRI_WEB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_WEB")', ls=':', c='r')
ax.legend(loc='upper right', prop={'size': 14})
ax.set_xlim(0, 6)
ax.set_ylim(0, 1000)
ax.set_xticks(np.arange(0, 7, 1))
ax.set_yticks(np.arange(0, 1100.0, 100.0))
ax.set_xlabel('$N_e$, $10^5 \\cdot cm^{-3}$')
ax.set_ylabel('h, km')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.grid()
plt.savefig('./Comparison Ne 25.06.2023 05.00 UT 52.8811 N 103.256 E.png', bbox_inches='tight', dpi=600)
plt.close()

fig, ax = plt.subplots(1, 1, figsize=(30*cm, 25*cm))
fig.tight_layout()
plt.suptitle(t = 'Comparison 25.06.2023 05:00 UT 52.8811$\\degree$ N 103.256$\\degree$ E',
    fontsize=17, fontweight='bold', y=1.02)
# ax.plot(WEB_Tn_K, WEB_h_km, lw=3, label='Web version of IRI-2016')
ax.plot(WEB_Tn_K, WEB_h_km, lw=3, label='Version of IRI-2016 from $\\bf iritest.for$')
ax.plot(IRI_SUB_Tn_K, IRI_SUB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_SUB")', ls='--', c='g')
ax.plot(IRI_WEB_Tn_K, IRI_WEB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_WEB")', ls=':', c='r')
ax.legend(loc='upper left', prop={'size': 14})
ax.set_ylim(0, 1000)
ax.set_yticks(np.arange(0, 1100.0, 100.0))
ax.set_xlabel('$T_n$, %*10')
ax.set_ylabel('h, km')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.grid()
plt.savefig('./Comparison Tn 25.06.2023 05.00 UT 52.8811 N 103.256 E.png', bbox_inches='tight', dpi=600)
plt.close()

fig, ax = plt.subplots(1, 1, figsize=(30*cm, 25*cm))
fig.tight_layout()
plt.suptitle(t = 'Comparison 25.06.2023 05:00 UT 52.8811$\\degree$ N 103.256$\\degree$ E',
    fontsize=17, fontweight='bold', y=1.02)
# ax.plot(WEB_Ti_K, WEB_h_km, lw=3, label='Web version of IRI-2016')
ax.plot(WEB_Ti_K, WEB_h_km, lw=3, label='Version of IRI-2016 from $\\bf iritest.for$')
ax.plot(IRI_SUB_Ti_K, IRI_SUB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_SUB")', ls='--', c='g')
ax.plot(IRI_WEB_Ti_K, IRI_WEB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_WEB")', ls=':', c='r')
ax.legend(loc='upper left', prop={'size': 14})
ax.set_ylim(0, 1000)
ax.set_yticks(np.arange(0, 1100.0, 100.0))
ax.set_xlabel('$T_i$, %*10')
ax.set_ylabel('h, km')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.grid()
plt.savefig('./Comparison Ti 25.06.2023 05.00 UT 52.8811 N 103.256 E.png', bbox_inches='tight', dpi=600)
plt.close()

fig, ax = plt.subplots(1, 1, figsize=(30*cm, 25*cm))
fig.tight_layout()
plt.suptitle(t = 'Comparison 25.06.2023 05:00 UT 52.8811$\\degree$ N 103.256$\\degree$ E',
    fontsize=17, fontweight='bold', y=1.02)
# ax.plot(WEB_Te_K, WEB_h_km, lw=3, label='Web version of IRI-2016')
ax.plot(WEB_Te_K, WEB_h_km, lw=3, label='Version of IRI-2016 from $\\bf iritest.for$')
ax.plot(IRI_SUB_Te_K, IRI_SUB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_SUB")', ls='--', c='g')
ax.plot(IRI_WEB_Te_K, IRI_WEB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_WEB")', ls=':', c='r')
ax.legend(loc='upper left', prop={'size': 14})
ax.set_ylim(0, 1000)
ax.set_yticks(np.arange(0, 1100.0, 100.0))
ax.set_xlabel('$T_e$, %*10')
ax.set_ylabel('h, km')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.grid()
plt.savefig('./Comparison Te 25.06.2023 05.00 UT 52.8811 N 103.256 E.png', bbox_inches='tight', dpi=600)
plt.close()

fig, ax = plt.subplots(1, 1, figsize=(30*cm, 25*cm))
fig.tight_layout()
plt.suptitle(t = 'Comparison 25.06.2023 05:00 UT 52.8811$\\degree$ N 103.256$\\degree$ E',
    fontsize=17, fontweight='bold', y=1.02)
# ax.plot(WEB_O, WEB_h_km, lw=3, label='Web version of IRI-2016')
ax.plot(WEB_O, WEB_h_km, lw=3, label='Version of IRI-2016 from $\\bf iritest.for$')
ax.plot(IRI_SUB_N_O, IRI_SUB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_SUB")', ls='--', c='g')
ax.plot(IRI_WEB_N_O, IRI_WEB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_WEB")', ls=':', c='r')
ax.legend(loc='upper left', prop={'size': 14})
ax.set_ylim(0, 1000)
ax.set_yticks(np.arange(0, 1100.0, 100.0))
ax.set_xlabel('$N_{O^+}$, %*10')
ax.set_ylabel('h, km')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.grid()
plt.savefig('./Comparison O+ 25.06.2023 05.00 UT 52.8811 N 103.256 E.png', bbox_inches='tight', dpi=600)
plt.close()

fig, ax = plt.subplots(1, 1, figsize=(30*cm, 25*cm))
fig.tight_layout()
plt.suptitle(t = 'Comparison 25.06.2023 05:00 UT 52.8811$\\degree$ N 103.256$\\degree$ E',
    fontsize=17, fontweight='bold', y=1.02)
# ax.plot(WEB_N, WEB_h_km, lw=3, label='Web version of IRI-2016')
ax.plot(WEB_N, WEB_h_km, lw=3, label='Version of IRI-2016 from $\\bf iritest.for$')
ax.plot(IRI_SUB_N_N, IRI_SUB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_SUB")', ls='--', c='g')
ax.plot(IRI_WEB_N_N, IRI_WEB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_WEB")', ls=':', c='r')
ax.legend(loc='upper left', prop={'size': 14})
ax.set_ylim(0, 1000)
ax.set_yticks(np.arange(0, 1100.0, 100.0))
ax.set_xlabel('$N_{N^+}$, %*10')
ax.set_ylabel('h, km')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.grid()
plt.savefig('./Comparison N+ 25.06.2023 05.00 UT 52.8811 N 103.256 E.png', bbox_inches='tight', dpi=600)
plt.close()

fig, ax = plt.subplots(1, 1, figsize=(30*cm, 25*cm))
fig.tight_layout()
plt.suptitle(t = 'Comparison 25.06.2023 05:00 UT 52.8811$\\degree$ N 103.256$\\degree$ E',
    fontsize=17, fontweight='bold', y=1.02)
# ax.plot(WEB_H, WEB_h_km, lw=3, label='Web version of IRI-2016')
ax.plot(WEB_H, WEB_h_km, lw=3, label='Version of IRI-2016 from $\\bf iritest.for$')
ax.plot(IRI_SUB_N_H, IRI_SUB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_SUB")', ls='--', c='g')
ax.plot(IRI_WEB_N_H, IRI_WEB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_WEB")', ls=':', c='r')
ax.legend(loc='upper left', prop={'size': 14})
ax.set_ylim(0, 1000)
ax.set_yticks(np.arange(0, 1100.0, 100.0))
ax.set_xlabel('$N_{H^+}$, %*10')
ax.set_ylabel('h, km')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.grid()
plt.savefig('./Comparison H+ 25.06.2023 05.00 UT 52.8811 N 103.256 E.png', bbox_inches='tight', dpi=600)
plt.close()

fig, ax = plt.subplots(1, 1, figsize=(30*cm, 25*cm))
fig.tight_layout()
plt.suptitle(t = 'Comparison 25.06.2023 05:00 UT 52.8811$\\degree$ N 103.256$\\degree$ E',
    fontsize=17, fontweight='bold', y=1.02)
# ax.plot(WEB_He, WEB_h_km, lw=3, label='Web version of IRI-2016')
ax.plot(WEB_He, WEB_h_km, lw=3, label='Version of IRI-2016 from $\\bf iritest.for$')
ax.plot(IRI_SUB_N_He, IRI_SUB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_SUB")', ls='--', c='g')
ax.plot(IRI_WEB_N_He, IRI_WEB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_WEB")', ls=':', c='r')
ax.legend(loc='lower right', prop={'size': 14})
ax.set_ylim(0, 1000)
ax.set_yticks(np.arange(0, 1100.0, 100.0))
ax.set_xlabel('$N_{He^+}$, %*10')
ax.set_ylabel('h, km')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.grid()
plt.savefig('./Comparison He+ 25.06.2023 05.00 UT 52.8811 N 103.256 E.png', bbox_inches='tight', dpi=600)
plt.close()

fig, ax = plt.subplots(1, 1, figsize=(30*cm, 25*cm))
fig.tight_layout()
plt.suptitle(t = 'Comparison 25.06.2023 05:00 UT 52.8811$\\degree$ N 103.256$\\degree$ E',
    fontsize=17, fontweight='bold', y=1.02)
# ax.plot(WEB_O2, WEB_h_km, lw=3, label='Web version of IRI-2016')
ax.plot(WEB_O2, WEB_h_km, lw=3, label='Version of IRI-2016 from $\\bf iritest.for$')
ax.plot(IRI_SUB_N_O2, IRI_SUB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_SUB")', ls='--', c='g')
ax.plot(IRI_WEB_N_O2, IRI_WEB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_WEB")', ls=':', c='r')
ax.legend(loc='upper right', prop={'size': 14})
ax.set_ylim(0, 1000)
ax.set_yticks(np.arange(0, 1100.0, 100.0))
ax.set_xlabel('$N_{O_2^+}$, %*10')
ax.set_ylabel('h, km')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.grid()
plt.savefig('./Comparison O2+ 25.06.2023 05.00 UT 52.8811 N 103.256 E.png', bbox_inches='tight', dpi=600)
plt.close()

fig, ax = plt.subplots(1, 1, figsize=(30*cm, 25*cm))
fig.tight_layout()
plt.suptitle(t = 'Comparison 25.06.2023 05:00 UT 52.8811$\\degree$ N 103.256$\\degree$ E',
    fontsize=17, fontweight='bold', y=1.02)
# ax.plot(WEB_NO, WEB_h_km, lw=3, label='Web version of IRI-2016')
ax.plot(WEB_NO, WEB_h_km, lw=3, label='Version of IRI-2016 from $\\bf iritest.for$')
ax.plot(IRI_SUB_N_NO, IRI_SUB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_SUB")', ls='--', c='g')
ax.plot(IRI_WEB_N_NO, IRI_WEB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_WEB")', ls=':', c='r')
ax.legend(loc='upper right', prop={'size': 14})
ax.set_ylim(0, 1000)
ax.set_yticks(np.arange(0, 1100.0, 100.0))
ax.set_xlabel('$N_{NO^+}$, %*10')
ax.set_ylabel('h, km')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.grid()
plt.savefig('./Comparison NO+ 25.06.2023 05.00 UT 52.8811 N 103.256 E.png', bbox_inches='tight', dpi=600)
plt.close()

fig, ax = plt.subplots(1, 1, figsize=(30*cm, 25*cm))
fig.tight_layout()
plt.suptitle(t = 'Comparison 25.06.2023 05:00 UT 52.8811$\\degree$ N 103.256$\\degree$ E',
    fontsize=17, fontweight='bold', y=1.02)
# ax.plot(WEB_Clust, WEB_h_km, lw=3, label='Web version of IRI-2016')
ax.plot(WEB_Clust, WEB_h_km, lw=3, label='Version of IRI-2016 from $\\bf iritest.for$')
ax.plot(IRI_SUB_N_Clust, IRI_SUB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_SUB")', ls='--', c='g')
ax.plot(IRI_WEB_N_Clust, IRI_WEB_h_km, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_WEB")', ls=':', c='r')
ax.legend(loc='upper right', prop={'size': 14})
ax.set_ylim(0, 1000)
ax.set_yticks(np.arange(0, 1100.0, 100.0))
ax.set_xlabel('Cluster ions, %*10')
ax.set_ylabel('h, km')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.grid()
plt.savefig('./Comparison Cluster 25.06.2023 05.00 UT 52.8811 N 103.256 E.png', bbox_inches='tight', dpi=600)
plt.close()

IRI_SUB_NmF2_m3 = []
IRI_SUB_hmF2_km = []
IRI_SUB_NmF1_m3 = []
IRI_SUB_hmF1_km = []
IRI_SUB_NmE_m3 = []
IRI_SUB_hmE_km = []
IRI_SUB_M3000F2 = []
with open(IRI_SUB_pars_filename, 'r') as f:
     for i, line in enumerate(f): #File reading
        if line != '\n':
            if i != 0:
                s1 = line.split('\t')
                IRI_SUB_NmF2_m3.append(float(s1[1]))
                IRI_SUB_hmF2_km.append(float(s1[2]))
                IRI_SUB_NmF1_m3.append(float(s1[3]))
                IRI_SUB_hmF1_km.append(float(s1[4]))
                IRI_SUB_NmE_m3.append(float(s1[5]))
                IRI_SUB_hmE_km.append(float(s1[6]))
                IRI_SUB_M3000F2.append(float(s1[7]))
IRI_WEB_NmF2_m3 = []
IRI_WEB_hmF2_km = []
IRI_WEB_NmF1_m3 = []
IRI_WEB_hmF1_km = []
IRI_WEB_NmE_m3 = []
IRI_WEB_hmE_km = []
IRI_WEB_M3000F2 = []
IRI_WEB_TECU = []
with open(IRI_WEB_pars_filename, 'r') as f:
     for i, line in enumerate(f): #File reading
        if line != '\n':
            if i != 0:
                s1 = line.split('\t')
                IRI_WEB_NmF2_m3.append(float(s1[1]))
                IRI_WEB_hmF2_km.append(float(s1[2]))
                IRI_WEB_NmF1_m3.append(float(s1[3]))
                IRI_WEB_hmF1_km.append(float(s1[4]))
                IRI_WEB_NmE_m3.append(float(s1[5]))
                IRI_WEB_hmE_km.append(float(s1[6]))
                IRI_WEB_M3000F2.append(float(s1[7]))
                IRI_WEB_TECU.append(float(s1[8]))

fig, (ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8) = plt.subplots(8, 1, sharex=True, figsize=(40*cm, 40*cm))
plt.suptitle(t = 'Comparison multi-threaded parameters 25.06.2023 05.00 UT 52.8811 N 103.256 E', fontsize=17, fontweight='bold')
plt.subplots_adjust(hspace = 0.3)
plt.tight_layout()

ax1.plot(IRI_SUB_NmF2_m3, lw=3, label='IRI_SUB')
ax1.plot(IRI_WEB_NmF2_m3, lw=3, label='IRI_WEB', ls='--', c='r')
ax1.legend(loc='upper center', prop={'size': 14})
ax1.set_ylabel('$N_{mF2}$, $m^{-3}$')
ax1.spines['right'].set_visible(False)
ax1.spines['top'].set_visible(False)
ax1.grid()

ax2.plot(IRI_SUB_hmF2_km, lw=3, label='IRI_SUB')
ax2.plot(IRI_WEB_hmF2_km, lw=3, label='IRI_WEB', ls='--', c='r')
ax2.set_ylabel('$h_{mF2}$, km')
ax2.spines['right'].set_visible(False)
ax2.spines['top'].set_visible(False)
ax2.grid()

ax3.plot(IRI_SUB_NmF1_m3, lw=3, label='IRI_SUB')
ax3.plot(IRI_WEB_NmF1_m3, lw=3, label='IRI_WEB', ls='--', c='r')
ax3.set_ylabel('$N_{mF1}$, $m^{-3}$')
ax3.spines['right'].set_visible(False)
ax3.spines['top'].set_visible(False)
ax3.grid()

ax4.plot(IRI_SUB_hmF1_km, lw=3, label='IRI_SUB')
ax4.plot(IRI_WEB_hmF1_km, lw=3, label='IRI_WEB', ls='--', c='r')
ax4.set_ylabel('$h_{mF1}$, km')
ax4.spines['right'].set_visible(False)
ax4.spines['top'].set_visible(False)
ax4.grid()

ax5.plot(IRI_SUB_NmE_m3, lw=3, label='IRI_SUB')
ax5.plot(IRI_WEB_NmE_m3, lw=3, label='IRI_WEB', ls='--', c='r')
ax5.set_ylabel('$N_{mE}$, $m^{-3}$')
ax5.spines['right'].set_visible(False)
ax5.spines['top'].set_visible(False)
ax5.grid()

ax6.plot(IRI_SUB_hmE_km, lw=3, label='IRI_SUB')
ax6.plot(IRI_WEB_hmE_km, lw=3, label='IRI_WEB', ls='--', c='r')
ax6.set_ylabel('$h_{mE}$, km')
ax6.spines['right'].set_visible(False)
ax6.spines['top'].set_visible(False)
ax6.grid()

ax7.plot(IRI_SUB_M3000F2, lw=3, label='IRI_SUB')
ax7.plot(IRI_WEB_M3000F2, lw=3, label='IRI_WEB', ls='--', c='r')
ax7.set_ylabel('M(3000)F2')
ax7.spines['right'].set_visible(False)
ax7.spines['top'].set_visible(False)
ax7.grid()

ax8.plot(IRI_WEB_TECU, lw=3, label='IRI_WEB', ls='--', c='r')
ax8.set_xlabel('Number of thread')
ax8.set_ylabel('TEC, TECU')
ax8.spines['right'].set_visible(False)
ax8.spines['top'].set_visible(False)
ax8.grid()

plt.savefig('./Comparison multi-threaded parameters IRI-2016 52.8811 deg N 103.256 deg E 25.06.2023 5.00 UT.png', bbox_inches='tight')
plt.close()

count = 0
for p in IRI_SUB_dats_path.rglob("*.dat"):
    count += 1
    hi_km = []
    Nei_cm3 = []
    Bi_nTl = []
    Ii_deg = []
    Di_deg = []
    Tn_K = []
    Ti_K = []
    Te_K = []
    N_O = []
    N_N = []
    N_H = []
    N_He = []
    N_O2 = []
    N_NO = []
    N_Clust = []
    with open(p, 'r') as f:
        for i, line in enumerate(f): #File reading
            if line != '\n':
                if i != 0:
                    s1 = line.split('\t')
                    hi_km.append(float(s1[0]))
                    Nei_cm3.append(float(s1[1]))
                    Bi_nTl.append(float(s1[2]))
                    Ii_deg.append(float(s1[3]))
                    Di_deg.append(float(s1[4]))
                    Tn_K.append(float(s1[5]))
                    Ti_K.append(float(s1[6]))
                    Te_K.append(float(s1[7]))
                    N_O.append(float(s1[8]))
                    N_N.append(float(s1[9]))
                    N_H.append(float(s1[10]))
                    N_He.append(float(s1[11]))
                    N_O2.append(float(s1[12]))
                    N_NO.append(float(s1[13]))
                    N_Clust.append(float(s1[14]))

    fig, (ax1, ax2, ax3, ax4) = plt.subplots(1, 4, sharey=False, figsize=(35*cm, 25*cm))
    plt.suptitle(t = 'Comparison "'+p.name[:]+f'"\nwith "{Path(IRI_SUB_single_threaded_filename).name}"',
        fontsize=17, fontweight='bold')
    plt.subplots_adjust(wspace = 0.5)

    ax1.plot(IRI_SUB_Ne_cm3, IRI_SUB_h_km, lw=3, label='Single threaded mode')
    ax1.plot(Nei_cm3, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax1.legend(loc='lower center', prop={'size': 14}, bbox_to_anchor = (2.25, -0.2))
    ax1.set_xlim(0, 6)
    ax1.set_ylim(0, 1000)
    ax1.set_xticks(np.arange(0, 7, 1))
    ax1.set_yticks(np.arange(0, 1100.0, 100.0))
    ax1.set_xlabel('$N_e$, $10^5 \\cdot cm^{-3}$')
    ax1.set_ylabel('h, km')
    ax1.spines['right'].set_visible(False)
    ax1.spines['top'].set_visible(False)
    ax1.grid()

    ax2.plot(B_nTl, IRI_SUB_h_km, lw=3, label='Single threaded mode')
    ax2.plot(Bi_nTl, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax2.set_yticks(np.arange(0, 1100.0, 100.0))
    ax2.set_xlabel('$\\left| \\overrightarrow{B} \\right|$, nT')
    ax2.set_ylabel('h, km')
    ax2.set_ylim(0, 1000)
    ax2.spines['right'].set_visible(False)
    ax2.spines['top'].set_visible(False)
    ax2.grid()

    ax3.plot(I_deg, IRI_SUB_h_km, lw=3, label='Single threaded mode')
    ax3.plot(Ii_deg, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax3.set_yticks(np.arange(0, 1100.0, 100.0))
    ax3.set_xlabel('I, $\\degree$')
    ax3.set_ylabel('h, km')
    ax3.set_ylim(0, 1000)
    ax3.spines['right'].set_visible(False)
    ax3.spines['top'].set_visible(False)
    ax3.grid()

    ax4.plot(D_deg, IRI_SUB_h_km, lw=3, label='Single threaded mode')
    ax4.plot(Di_deg, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax4.set_yticks(np.arange(0, 1100.0, 100.0))
    ax4.set_xlabel('D, $\\degree$')
    ax4.set_ylabel('h, km')
    ax4.set_ylim(0, 1000)
    ax4.spines['right'].set_visible(False)
    ax4.spines['top'].set_visible(False)
    ax4.grid()

    plt.savefig(IRI_SUB_Ne_output_dir + f'{count}.png', bbox_inches='tight')
    plt.close()


    fig, (ax1, ax2, ax3) = plt.subplots(1, 3, sharey=True, figsize=(35*cm, 25*cm))
    plt.suptitle(t = 'Comparison "'+p.name[:]+f'"\nwith "{Path(IRI_SUB_single_threaded_filename).name}"',
        fontsize=17, fontweight='bold')
    plt.subplots_adjust(wspace = 0.2)

    ax1.plot(IRI_SUB_Tn_K, IRI_SUB_h_km, lw=3, label='Single threaded mode')
    ax1.plot(Tn_K, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax1.legend(loc='lower center', prop={'size': 14}, bbox_to_anchor = (2.25, -0.2))
    ax1.set_ylim(0, 1000)
    ax1.set_yticks(np.arange(0, 1100.0, 100.0))
    ax1.set_xlabel('$T_n$, K')
    ax1.set_ylabel('h, km')
    ax1.spines['right'].set_visible(False)
    ax1.spines['top'].set_visible(False)
    ax1.grid()

    ax2.plot(IRI_SUB_Ti_K, IRI_SUB_h_km, lw=3, label='Single threaded mode')
    ax2.plot(Ti_K, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax2.set_yticks(np.arange(0, 1100.0, 100.0))
    ax2.set_xlabel('$T_i$, K')
    ax2.set_ylabel('h, km')
    ax2.set_ylim(0, 1000)
    ax2.spines['right'].set_visible(False)
    ax2.spines['top'].set_visible(False)
    ax2.grid()

    ax3.plot(IRI_SUB_Te_K, IRI_SUB_h_km, lw=3, label='Single threaded mode')
    ax3.plot(Te_K, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax3.set_yticks(np.arange(0, 1100.0, 100.0))
    ax3.set_xlabel('$T_e$, K')
    ax3.set_ylabel('h, km')
    ax3.set_ylim(0, 1000)
    ax3.spines['right'].set_visible(False)
    ax3.spines['top'].set_visible(False)
    ax3.grid()

    plt.savefig(IRI_SUB_T_output_dir + f'{count}.png', bbox_inches='tight')
    plt.close()
    
    
    
    fig, (ax1, ax2, ax3, ax4, ax5, ax6, ax7) = plt.subplots(1, 7, sharey=True, figsize=(45*cm, 25*cm))
    plt.suptitle(t = 'Comparison "'+p.name[:]+f'"\nwith "{Path(IRI_SUB_single_threaded_filename).name}"',
        fontsize=17, fontweight='bold')
    plt.subplots_adjust(wspace = 0.3)

    ax1.plot(IRI_SUB_N_O, IRI_SUB_h_km, lw=3, label='Single threaded mode')
    ax1.plot(N_O, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax1.legend(loc='lower center', prop={'size': 14}, bbox_to_anchor = (2.25, -0.2))
    ax1.set_ylim(0, 1000)
    ax1.set_yticks(np.arange(0, 1100.0, 100.0))
    ax1.set_xlabel('$N_{O^+}$, %*10')
    ax1.set_ylabel('h, km')
    ax1.spines['right'].set_visible(False)
    ax1.spines['top'].set_visible(False)
    ax1.grid()

    ax2.plot(IRI_SUB_N_N, IRI_SUB_h_km, lw=3, label='Single threaded mode')
    ax2.plot(N_N, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax2.set_yticks(np.arange(0, 1100.0, 100.0))
    ax2.set_xlabel('$N_{N^+}$, %*10')
    ax2.set_ylabel('h, km')
    ax2.set_ylim(0, 1000)
    ax2.spines['right'].set_visible(False)
    ax2.spines['top'].set_visible(False)
    ax2.grid()

    ax3.plot(IRI_SUB_N_H, IRI_SUB_h_km, lw=3, label='Single threaded mode')
    ax3.plot(N_H, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax3.set_yticks(np.arange(0, 1100.0, 100.0))
    ax3.set_xlabel('$N_{H^+}$, %*10')
    ax3.set_ylabel('h, km')
    ax3.set_ylim(0, 1000)
    ax3.spines['right'].set_visible(False)
    ax3.spines['top'].set_visible(False)
    ax3.grid()

    ax4.plot(IRI_SUB_N_He, IRI_SUB_h_km, lw=3, label='Single threaded mode')
    ax4.plot(N_He, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax4.set_yticks(np.arange(0, 1100.0, 100.0))
    ax4.set_xlabel('$N_{He^+}$, %*10')
    ax4.set_ylabel('h, km')
    ax4.set_ylim(0, 1000)
    ax4.spines['right'].set_visible(False)
    ax4.spines['top'].set_visible(False)
    ax4.grid()

    ax5.plot(IRI_SUB_N_O2, IRI_SUB_h_km, lw=3, label='Single threaded mode')
    ax5.plot(N_O2, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax5.set_yticks(np.arange(0, 1100.0, 100.0))
    ax5.set_xlabel('$N_{O_2^+}$, %*10')
    ax5.set_ylabel('h, km')
    ax5.set_ylim(0, 1000)
    ax5.spines['right'].set_visible(False)
    ax5.spines['top'].set_visible(False)
    ax5.grid()

    ax6.plot(IRI_SUB_N_NO, IRI_SUB_h_km, lw=3, label='Single threaded mode')
    ax6.plot(N_NO, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax6.set_yticks(np.arange(0, 1100.0, 100.0))
    ax6.set_xlabel('$N_{NO^+}$, %*10')
    ax6.set_ylabel('h, km')
    ax6.set_ylim(0, 1000)
    ax6.spines['right'].set_visible(False)
    ax6.spines['top'].set_visible(False)
    ax6.grid()

    ax7.plot(IRI_SUB_N_Clust, IRI_SUB_h_km, lw=3, label='Single threaded mode')
    ax7.plot(N_Clust, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax7.set_yticks(np.arange(0, 1100.0, 100.0))
    ax7.set_xlabel('Cluster ions, %*10')
    ax7.set_ylabel('h, km')
    ax7.set_ylim(0, 1000)
    ax7.spines['right'].set_visible(False)
    ax7.spines['top'].set_visible(False)
    ax7.grid()

    plt.savefig(IRI_SUB_Ni_output_dir + f'{count}.png', bbox_inches='tight')
    plt.close()

count = 0
for p in IRI_WEB_dats_path.rglob("*.dat"):
    count += 1
    hi_km = []
    Nei_cm3 = []
    Tn_K = []
    Ti_K = []
    Te_K = []
    N_O = []
    N_N = []
    N_H = []
    N_He = []
    N_O2 = []
    N_NO = []
    N_Clust = []
    with open(p, 'r') as f:
        for i, line in enumerate(f): #File reading
            if line != '\n':
                if i != 0:
                    s1 = line.split('\t')
                    hi_km.append(float(s1[0]))
                    Nei_cm3.append(float(s1[1]))
                    Tn_K.append(float(s1[2]))
                    Ti_K.append(float(s1[3]))
                    Te_K.append(float(s1[4]))
                    N_O.append(float(s1[5]))
                    N_N.append(float(s1[6]))
                    N_H.append(float(s1[7]))
                    N_He.append(float(s1[8]))
                    N_O2.append(float(s1[9]))
                    N_NO.append(float(s1[10]))
                    N_Clust.append(float(s1[11]))

    fig, ax5 = plt.subplots(1, 1, figsize=(30*cm, 25*cm))
    plt.suptitle(t = 'Comparison "'+p.name[:]+f'"\nwith "{Path(IRI_WEB_single_threaded_filename).name}"',
        fontsize=17, fontweight='bold')
    plt.subplots_adjust(wspace = 0.5)
    ax5.plot(IRI_WEB_Ne_cm3, IRI_WEB_h_km, lw=3, label='Single threaded mode')
    ax5.plot(Nei_cm3, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax5.legend(loc='upper right', prop={'size': 14})
    ax5.set_xlim(0, 6)
    ax5.set_ylim(0, 1000)
    ax5.set_xticks(np.arange(0, 7, 1))
    ax5.set_yticks(np.arange(0, 1100.0, 100.0))
    ax5.set_xlabel('$N_e$, $10^5 \\cdot cm^{-3}$')
    ax5.set_ylabel('h, km')
    ax5.spines['right'].set_visible(False)
    ax5.spines['top'].set_visible(False)
    ax5.grid()
    plt.savefig(IRI_WEB_Ne_output_dir + f'{count}.png', bbox_inches='tight')
    plt.close()


    fig, (ax1, ax2, ax3) = plt.subplots(1, 3, sharey=True, figsize=(35*cm, 25*cm))
    plt.suptitle(t = 'Comparison "'+p.name[:]+f'"\nwith "{Path(IRI_WEB_single_threaded_filename).name}"',
        fontsize=17, fontweight='bold')
    plt.subplots_adjust(wspace = 0.2)

    ax1.plot(IRI_WEB_Tn_K, IRI_WEB_h_km, lw=3, label='Single threaded mode')
    ax1.plot(Tn_K, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax1.legend(loc='lower center', prop={'size': 14}, bbox_to_anchor = (2.25, -0.2))
    ax1.set_ylim(0, 1000)
    ax1.set_yticks(np.arange(0, 1100.0, 100.0))
    ax1.set_xlabel('$T_n$, K')
    ax1.set_ylabel('h, km')
    ax1.spines['right'].set_visible(False)
    ax1.spines['top'].set_visible(False)
    ax1.grid()

    ax2.plot(IRI_WEB_Ti_K, IRI_WEB_h_km, lw=3, label='Single threaded mode')
    ax2.plot(Ti_K, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax2.set_yticks(np.arange(0, 1100.0, 100.0))
    ax2.set_xlabel('$T_i$, K')
    ax2.set_ylabel('h, km')
    ax2.set_ylim(0, 1000)
    ax2.spines['right'].set_visible(False)
    ax2.spines['top'].set_visible(False)
    ax2.grid()

    ax3.plot(IRI_WEB_Te_K, IRI_WEB_h_km, lw=3, label='Single threaded mode')
    ax3.plot(Te_K, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax3.set_yticks(np.arange(0, 1100.0, 100.0))
    ax3.set_xlabel('$T_e$, K')
    ax3.set_ylabel('h, km')
    ax3.set_ylim(0, 1000)
    ax3.spines['right'].set_visible(False)
    ax3.spines['top'].set_visible(False)
    ax3.grid()

    plt.savefig(IRI_WEB_T_output_dir + f'{count}.png', bbox_inches='tight')
    plt.close()
    
    
    
    fig, (ax1, ax2, ax3, ax4, ax5, ax6, ax7) = plt.subplots(1, 7, sharey=True, figsize=(45*cm, 25*cm))
    plt.suptitle(t = 'Comparison "'+p.name[:]+f'"\nwith "{Path(IRI_WEB_single_threaded_filename).name}"',
        fontsize=17, fontweight='bold')
    plt.subplots_adjust(wspace = 0.3)

    ax1.plot(IRI_WEB_N_O, IRI_WEB_h_km, lw=3, label='Single threaded mode')
    ax1.plot(N_O, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax1.legend(loc='lower center', prop={'size': 14}, bbox_to_anchor = (2.25, -0.2))
    ax1.set_ylim(0, 1000)
    ax1.set_yticks(np.arange(0, 1100.0, 100.0))
    ax1.set_xlabel('$N_{O^+}$, %*10')
    ax1.set_ylabel('h, km')
    ax1.spines['right'].set_visible(False)
    ax1.spines['top'].set_visible(False)
    ax1.grid()

    ax2.plot(IRI_WEB_N_N, IRI_WEB_h_km, lw=3, label='Single threaded mode')
    ax2.plot(N_N, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax2.set_yticks(np.arange(0, 1100.0, 100.0))
    ax2.set_xlabel('$N_{N^+}$, %*10')
    ax2.set_ylabel('h, km')
    ax2.set_ylim(0, 1000)
    ax2.spines['right'].set_visible(False)
    ax2.spines['top'].set_visible(False)
    ax2.grid()

    ax3.plot(IRI_WEB_N_H, IRI_WEB_h_km, lw=3, label='Single threaded mode')
    ax3.plot(N_H, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax3.set_yticks(np.arange(0, 1100.0, 100.0))
    ax3.set_xlabel('$N_{H^+}$, %*10')
    ax3.set_ylabel('h, km')
    ax3.set_ylim(0, 1000)
    ax3.spines['right'].set_visible(False)
    ax3.spines['top'].set_visible(False)
    ax3.grid()

    ax4.plot(IRI_WEB_N_He, IRI_WEB_h_km, lw=3, label='Single threaded mode')
    ax4.plot(N_He, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax4.set_yticks(np.arange(0, 1100.0, 100.0))
    ax4.set_xlabel('$N_{He^+}$, %*10')
    ax4.set_ylabel('h, km')
    ax4.set_ylim(0, 1000)
    ax4.spines['right'].set_visible(False)
    ax4.spines['top'].set_visible(False)
    ax4.grid()

    ax5.plot(IRI_WEB_N_O2, IRI_WEB_h_km, lw=3, label='Single threaded mode')
    ax5.plot(N_O2, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax5.set_yticks(np.arange(0, 1100.0, 100.0))
    ax5.set_xlabel('$N_{O_2^+}$, %*10')
    ax5.set_ylabel('h, km')
    ax5.set_ylim(0, 1000)
    ax5.spines['right'].set_visible(False)
    ax5.spines['top'].set_visible(False)
    ax5.grid()

    ax6.plot(IRI_WEB_N_NO, IRI_WEB_h_km, lw=3, label='Single threaded mode')
    ax6.plot(N_NO, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax6.set_yticks(np.arange(0, 1100.0, 100.0))
    ax6.set_xlabel('$N_{NO^+}$, %*10')
    ax6.set_ylabel('h, km')
    ax6.set_ylim(0, 1000)
    ax6.spines['right'].set_visible(False)
    ax6.spines['top'].set_visible(False)
    ax6.grid()

    ax7.plot(IRI_WEB_N_Clust, IRI_WEB_h_km, lw=3, label='Single threaded mode')
    ax7.plot(N_Clust, hi_km, lw=3, label='Multi-threaded mode', ls='--')
    ax7.set_yticks(np.arange(0, 1100.0, 100.0))
    ax7.set_xlabel('Cluster ions, %*10')
    ax7.set_ylabel('h, km')
    ax7.set_ylim(0, 1000)
    ax7.spines['right'].set_visible(False)
    ax7.spines['top'].set_visible(False)
    ax7.grid()

    plt.savefig(IRI_WEB_Ni_output_dir + f'{count}.png', bbox_inches='tight')
    plt.close()
