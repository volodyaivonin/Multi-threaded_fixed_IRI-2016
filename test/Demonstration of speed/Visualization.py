from datetime import datetime
import time
from scipy.interpolate import griddata
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter, HourLocator
from matplotlib.ticker import AutoMinorLocator

def calc_dechour(dt):
    return dt.hour + dt.minute/60 + dt.second/3600 + dt.microsecond/36.E8
def calc_decim_doy(dt):
    return dt.timetuple().tm_yday + calc_dechour(dt)/24

def get_timestring_from_dechour(dechour):
    hours = int(dechour)
    d = dechour - float(hours)
    d *= 60.0
    minutes = int(d)
    d -= float(minutes)
    seconds = int(60.0*d)
    d *= 60.0
    d -= float(seconds)
    microseconds = 1000*int(1000.0*d)
    return "%02d:%02d:%02d.%06d+0000" % (hours, minutes, seconds, microseconds)

Speed_demonstration = 'Speed_demonstration.dat'
single_test_filename = 'speed_test_IRI_WEB_IRI-2016_single_52.881deg_N_103.256deg_E_01.02.2022.dat'
multi_test_filename = 'speed_test_IRI_WEB_IRI-2016_multi_52.881deg_N_103.256deg_E_01.02.2022.dat'

lat = float(single_test_filename.split('_')[6].split('deg')[0])
longit = float(single_test_filename.split('_')[8].split('deg')[0])

cm = 1/2.54
dpi = 600
plt.rcParams["font.family"] = "Times New Roman"
plt.rcParams["font.size"] = 24
matplotlib.use('Agg')
#Setting up the colormap:
black = (0, 0, 0)
brown = (0.6, 0.3, 0)
blue = (0, 0, 1)
cyan = (0.2, 1, 1)
dark_green = (0, 0.6, 0)
yellow = (1, 1, 0)
orange = (1, 0.6, 0.2)
red = (1, 0, 0)
pink = (1, 0.6, 0.6)
white = (1, 1, 1)
colors = [black, blue, blue, cyan, dark_green, dark_green, yellow, orange, orange, red, red, pink, white, white]
my_cmap = matplotlib.colors.LinearSegmentedColormap.from_list('my_colormap', colors, N=1000)
my_cmap.set_bad(brown)#Для отсутствующих (np.masked) данных

#Data reading:
Threads_number = []
Execution_time_s = []
with open(Speed_demonstration, 'r') as f:
    for i, line in enumerate(f):
        if line != '\n':
            if i != 0:
                s = line.split('\t')
                Threads_number.append(int(s[0]))
                Execution_time_s.append(float(s[1].split('\n')[0]))

fig, ax1 = plt.subplots(1, 1, figsize=(20*cm, 10*cm), dpi=dpi, num=1, clear=True)
ax1.set_title('Execution time of simulation 1,441 IRI altitude\n(916 points) profiles', fontsize=24, fontweight='bold')
fig.tight_layout()
ax1.plot(Threads_number, Execution_time_s, lw=3, marker='.', markersize=20)
min_ex_time = min(Execution_time_s)
min_ex_time_thread_num = Threads_number[Execution_time_s.index(min_ex_time)]
ax1.plot(min_ex_time_thread_num, min_ex_time, c='r', marker='.', markersize=20)
ax1.annotate(f'minimum execution time: {min_ex_time:.1f} s', xy=(min_ex_time_thread_num, 1.2*min_ex_time), xytext=(min_ex_time_thread_num/1.8, 47),
    arrowprops=dict(edgecolor='red', facecolor='red', shrink=0.03), color='red')
max_ex_time = max(Execution_time_s)
max_ex_time_thread_num = Threads_number[Execution_time_s.index(max_ex_time)]
ax1.plot(max_ex_time_thread_num, max_ex_time, c='r', marker='.', markersize=20)
ax1.annotate(f'maximum execution time: {max_ex_time:.1f} s', xy=(1.2*max_ex_time_thread_num, max_ex_time), xytext=(6*max_ex_time_thread_num, 0.85*max_ex_time),
    arrowprops=dict(edgecolor='red', facecolor='red', shrink=0.03), color='red')
ax1.set_xlabel('Threads amount', fontweight = 'bold')
ax1.set_xticks(range(0, 33, 4))
ax1.set_xlim(0, 32.5)
ax1.xaxis.set_minor_locator(AutoMinorLocator(2))
ax1.tick_params(axis="x", which="major", direction = 'inout', length=25, pad=5, width=2)
ax1.tick_params(axis="x", which="minor", direction = 'inout', length=8)
ax1.xaxis.grid(which='major', lw=2)
ax1.xaxis.grid(which='minor', lw=1, ls='--')
ax1.set_ylabel('Execution time, s', fontweight = 'bold')
ax1.set_ylim(0, 78)
ax1.set_yticks(np.linspace(0, 75, 6))
ax1.yaxis.set_major_formatter('{x:.0f}')
ax1.yaxis.set_minor_locator(AutoMinorLocator(2))
ax1.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, width=2)
ax1.tick_params(axis="y", which="minor", pad=5, direction = 'out', length=5, width=1)
ax1.yaxis.grid(which='major', lw=2)
ax1.yaxis.grid(which='minor', lw=1, ls='--')
ax1.spines['right'].set_visible(False)
ax1.spines['top'].set_visible(False)
ax1.spines['left'].set_position('zero')
ax1.spines['bottom'].set_position('zero')
plt.savefig('Execution time with different threads amount.png', bbox_inches='tight', dpi=dpi, transparent=True)
fig.clf()
plt.close(fig)

single_dt = []
single_doys = []
single_h_km = []
single_Ne_cm3 = []
single_Tn_K = []
single_Ti_K = []
single_Te_K = []
single_N_O_cm3 = []
single_N_N_cm3 = []
single_N_H_cm3 = []
single_N_He_cm3 = []
single_N_O2_cm3 = []
single_N_NO_cm3 = []
single_N_Clust_cm3 = []

start_time = time.time()
print('Reading a file created in single-threaded mode...')
n_times = 0
h_readed = False
with open(single_test_filename, 'r') as f:
    dt_UT = None
    previous_line = ''
    datestr = '01.02.2022'
    for i, line in enumerate(f):
        if line != '\n':
            if i != 0:
                s1 = line.split('\t')
                dt_UT = datetime.strptime(datestr + get_timestring_from_dechour(float(s1[0])), '%d.%m.%Y%H:%M:%S.%f%z')
                h = float(s1[1])
                if n_times == 0:
                    single_h_km.append(h)
                single_Ne_cm3.append(float(s1[2]))
                single_Tn_K.append(float(s1[3]))
                single_Ti_K.append(float(s1[4]))
                single_Te_K.append(float(s1[5]))
                single_N_O_cm3.append(float(s1[6]))
                single_N_N_cm3.append(float(s1[7]))
                single_N_H_cm3.append(float(s1[8]))
                single_N_He_cm3.append(float(s1[9]))
                if h <= 300:
                    single_N_O2_cm3.append(float(s1[10]))
                    single_N_NO_cm3.append(float(s1[11]))
                single_N_Clust_cm3.append(float(s1[12]))
        else:
            if (previous_line != '\n'):
                single_dt.append(dt_UT)
                single_doys.append(calc_decim_doy(dt_UT))
                n_times += 1
        previous_line=line
print(f'A file created in single-threaded mode was read in {time.time() - start_time} seconds')
print(f'Amount of x: {len(single_doys)}, amount of y: {len(single_h_km)}, amount of z: {len(single_Ne_cm3)}')

multi_dt = []
multi_doys = []
multi_h_km = []
multi_Ne_cm3 = []
multi_Tn_K = []
multi_Ti_K = []
multi_Te_K = []
multi_N_O_cm3 = []
multi_N_N_cm3 = []
multi_N_H_cm3 = []
multi_N_He_cm3 = []
multi_N_O2_cm3 = []
multi_N_NO_cm3 = []
multi_N_Clust_cm3 = []

start_time = time.time()
print('Reading a file created in multi-threaded mode...')
n_times = 0
h_readed = False
with open(multi_test_filename, 'r') as f:
    dt_UT = None
    previous_line = ''
    datestr = '01.02.2022'
    for i, line in enumerate(f):
        if line != '\n':
            if i != 0:
                s1 = line.split('\t')
                dt_UT = datetime.strptime(datestr + get_timestring_from_dechour(float(s1[0])), '%d.%m.%Y%H:%M:%S.%f%z')
                h = float(s1[1])
                if n_times == 0:
                    multi_h_km.append(h)
                multi_Ne_cm3.append(float(s1[2]))
                multi_Tn_K.append(float(s1[3]))
                multi_Ti_K.append(float(s1[4]))
                multi_Te_K.append(float(s1[5]))
                multi_N_O_cm3.append(float(s1[6]))
                multi_N_N_cm3.append(float(s1[7]))
                multi_N_H_cm3.append(float(s1[8]))
                multi_N_He_cm3.append(float(s1[9]))
                if h <= 300:
                    multi_N_O2_cm3.append(float(s1[10]))
                    multi_N_NO_cm3.append(float(s1[11]))
                multi_N_Clust_cm3.append(float(s1[12]))
        else:
            if (previous_line != '\n'):
                multi_dt.append(dt_UT)
                multi_doys.append(calc_decim_doy(dt_UT))
                n_times += 1
        previous_line=line
print(f'A file created in multi-threaded mode was read in {time.time() - start_time} seconds')
print(f'Amount of x: {len(multi_doys)}, amount of y: {len(multi_h_km)}, amount of z: {len(multi_Ne_cm3)}')

#Interpolation:
start_time = time.time()
print('Interpolation the data created in single-threaded mode...')
single_xlist = np.array(single_doys)
single_ylist = np.array(single_h_km)
single_X, single_Y = np.meshgrid(single_xlist, single_ylist)
single_points = np.array((single_X, single_Y)).T.reshape(-1, 2)
single_ylist2 = np.array([i for i in single_h_km if i<=300])
single_X2, single_Y2 = np.meshgrid(single_xlist, single_ylist2)
single_points2 = np.array((single_X2, single_Y2)).T.reshape(-1, 2)

single_grid_Ne = griddata(single_points, np.array(single_Ne_cm3), (single_X, single_Y), method='nearest')
single_grid_Tn = griddata(single_points, np.array(single_Tn_K), (single_X, single_Y), method='nearest')
single_grid_Ti = griddata(single_points, np.array(single_Ti_K), (single_X, single_Y), method='nearest')
single_grid_Te = griddata(single_points, np.array(single_Te_K), (single_X, single_Y), method='nearest')
single_grid_N_O = griddata(single_points, np.array(single_N_O_cm3), (single_X, single_Y), method='nearest')
single_grid_N_H = griddata(single_points, np.array(single_N_H_cm3), (single_X, single_Y), method='nearest')
single_grid_N_He = griddata(single_points, np.array(single_N_He_cm3), (single_X, single_Y), method='nearest')
single_grid_N_O2 = griddata(single_points2, np.array(single_N_O2_cm3), (single_X2, single_Y2), method='nearest')
single_grid_N_NO = griddata(single_points2, np.array(single_N_NO_cm3), (single_X2, single_Y2), method='nearest')
single_grid_Clust = griddata(single_points, np.array(single_N_Clust_cm3), (single_X, single_Y), method='nearest')
single_grid_N_N = griddata(single_points, np.array(single_N_N_cm3), (single_X, single_Y), method='nearest')
print(f'The data created in multi-threaded mode was interpolated in {time.time() - start_time} seconds')

start_time = time.time()
print('Interpolation the data created in multi-threaded mode...')
multi_xlist = np.array(multi_doys)
multi_ylist = np.array(multi_h_km)
multi_X, multi_Y = np.meshgrid(multi_xlist, multi_ylist)
multi_points = np.array((multi_X, multi_Y)).T.reshape(-1, 2)
multi_ylist2 = np.array([i for i in multi_h_km if i<=300])
multi_X2, multi_Y2 = np.meshgrid(multi_xlist, multi_ylist2)
multi_points2 = np.array((multi_X2, multi_Y2)).T.reshape(-1, 2)

multi_grid_Ne = griddata(multi_points, np.array(multi_Ne_cm3), (multi_X, multi_Y), method='nearest')
multi_grid_Tn = griddata(multi_points, np.array(multi_Tn_K), (multi_X, multi_Y), method='nearest')
multi_grid_Ti = griddata(multi_points, np.array(multi_Ti_K), (multi_X, multi_Y), method='nearest')
multi_grid_Te = griddata(multi_points, np.array(multi_Te_K), (multi_X, multi_Y), method='nearest')
multi_grid_N_O = griddata(multi_points, np.array(multi_N_O_cm3), (multi_X, multi_Y), method='nearest')
multi_grid_N_H = griddata(multi_points, np.array(multi_N_H_cm3), (multi_X, multi_Y), method='nearest')
multi_grid_N_He = griddata(multi_points, np.array(multi_N_He_cm3), (multi_X, multi_Y), method='nearest')
multi_grid_N_O2 = griddata(multi_points2, np.array(multi_N_O2_cm3), (multi_X2, multi_Y2), method='nearest')
multi_grid_N_NO = griddata(multi_points2, np.array(multi_N_NO_cm3), (multi_X2, multi_Y2), method='nearest')
multi_grid_Clust = griddata(multi_points, np.array(multi_N_Clust_cm3), (multi_X, multi_Y), method='nearest')
multi_grid_N_N = griddata(multi_points, np.array(multi_N_N_cm3), (multi_X, multi_Y), method='nearest')
print(f'The data created in multi-threaded mode was interpolated in {time.time() - start_time} seconds')

#Plotting colormaps:
print('Plotting the data created in single-threaded mode...')
start_time = time.time()

fig, ax=plt.subplots(1, 1, figsize=(20*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of electron density\n{lat:.1f}$\\bf \\degree$N {longit:.1f}$\\bf \\degree$E {single_dt[0].strftime('%B %d, %Y')}",
    fontsize=24, fontweight="bold", y=1.01)
cp = plt.imshow(single_grid_Ne, origin='lower', aspect='auto', cmap=my_cmap, vmin=0, vmax=7,
    extent=(single_dt[0], single_dt[-1], np.min(single_ylist), np.max(single_ylist)))
cbar = fig.colorbar(cp, pad=0.07) # Add a colorbar to a plot
cbar.set_label(label='$\\bf{N_e}$, $\\bf{10^5\\cdot cm^{-3}}$', labelpad=-57, fontweight="bold")
cbar.set_ticks(np.arange(0, 7.1, 1))
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.set_yticks(range(200, 2001, 200))
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(single_dt[0].strftime('%Y-%m-%d') + ' Ne altitude-time dynamics (single-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi,
    transparent=True)
plt.close()

fig, ax=plt.subplots(1, 1, figsize=(30*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of neutral temperature\n{single_dt[0].strftime('%Y-%m-%d')}", fontweight="bold")
cp = plt.imshow(single_grid_Tn, origin='lower', aspect='auto', cmap=my_cmap, vmin=500, vmax=1000,
    extent=(single_dt[0], single_dt[-1], np.min(single_ylist), np.max(single_ylist)))
cbar = fig.colorbar(cp, pad=0.05) # Add a colorbar to a plot
cbar.set_label(label='$\\bf{T_n}$, K', labelpad=-73, fontweight="bold")
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12, labelsize=16)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6, labelsize=14)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.set_yticks(range(200, 2001, 200))
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, labelsize=16)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5, labelsize=14)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(single_dt[0].strftime('%Y-%m-%d') + ' Tn altitude-time dynamics (single-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi)
plt.close()

fig, ax=plt.subplots(1, 1, figsize=(30*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of ion temperature\n{single_dt[0].strftime('%Y-%m-%d')}", fontweight="bold")
cp = plt.imshow(single_grid_Ti, origin='lower', aspect='auto', cmap=my_cmap, vmin=500, vmax=4200,
    extent=(single_dt[0], single_dt[-1], np.min(single_ylist), np.max(single_ylist)))
cbar = fig.colorbar(cp, pad=0.05) # Add a colorbar to a plot
cbar.set_label(label='$\\bf{T_i}$, K', labelpad=-73, fontweight="bold")
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12, labelsize=16)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6, labelsize=14)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.set_yticks(range(200, 2001, 200))
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, labelsize=16)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5, labelsize=14)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(single_dt[0].strftime('%Y-%m-%d') + ' Ti altitude-time dynamics (single-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi)
plt.close()

fig, ax=plt.subplots(1, 1, figsize=(30*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of electron temperature\n{single_dt[0].strftime('%Y-%m-%d')}", fontweight="bold")
cp = plt.imshow(single_grid_Te, origin='lower', aspect='auto', cmap=my_cmap, vmin=500, vmax=4200,
    extent=(single_dt[0], single_dt[-1], np.min(single_ylist), np.max(single_ylist)))
cbar = fig.colorbar(cp, pad=0.05) # Add a colorbar to a plot
cbar.set_label(label='$\\bf{T_e}$, K', labelpad=-73, fontweight="bold")
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12, labelsize=16)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6, labelsize=14)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.set_yticks(range(200, 2001, 200))
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, labelsize=16)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5, labelsize=14)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(single_dt[0].strftime('%Y-%m-%d') + ' Te altitude-time dynamics (single-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi)
plt.close()

fig, ax=plt.subplots(1, 1, figsize=(30*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of $\\bf O^+$ ions density\n{single_dt[0].strftime('%Y-%m-%d')}", fontweight="bold")
cp = plt.imshow(single_grid_N_O, origin='lower', aspect='auto', cmap=my_cmap, vmin=0, vmax=7,
    extent=(single_dt[0], single_dt[-1], np.min(single_ylist), np.max(single_ylist)))
cbar = fig.colorbar(cp, pad=0.05) # Add a colorbar to a plot
cbar.set_label(label='$\\bf{N_{O^+}}$, $\\bf{10^5\\cdot cm^{-3}}$', labelpad=-48, fontweight="bold")
cbar.set_ticks(np.arange(0, 7.1, 1))
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12, labelsize=16)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6, labelsize=14)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.set_yticks(range(200, 2001, 200))
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, labelsize=16)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5, labelsize=14)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(single_dt[0].strftime('%Y-%m-%d') + ' O+ altitude-time dynamics (single-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi)
plt.close()

fig, ax=plt.subplots(1, 1, figsize=(30*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of $\\bf H^+$ ions density\n{single_dt[0].strftime('%Y-%m-%d')}", fontweight="bold")
cp = plt.imshow(single_grid_N_H, origin='lower', aspect='auto', cmap=my_cmap, vmin=0, vmax=0.07,
    extent=(single_dt[0], single_dt[-1], np.min(single_ylist), np.max(single_ylist)))
cbar = fig.colorbar(cp, pad=0.05) # Add a colorbar to a plot
cbar.set_label(label='$\\bf N_{H^+}$, $\\bf{10^5\\cdot cm^{-3}}$', labelpad=-70, fontweight="bold")
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12, labelsize=16)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6, labelsize=14)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.set_yticks(range(200, 2001, 200))
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, labelsize=16)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5, labelsize=14)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(single_dt[0].strftime('%Y-%m-%d') + ' H+ altitude-time dynamics (single-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi)
plt.close()

fig, ax=plt.subplots(1, 1, figsize=(30*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of $\\bf He^+$ ions density\n{single_dt[0].strftime('%Y-%m-%d')}", fontweight="bold")
cp = plt.imshow(single_grid_N_He, origin='lower', aspect='auto', cmap=my_cmap, vmin=0, vmax=0.008,
    extent=(single_dt[0], single_dt[-1], np.min(single_ylist), np.max(single_ylist)))
cbar = fig.colorbar(cp, pad=0.05) # Add a colorbar to a plot
cbar.set_label(label='$\\bf N_{He^+}$, $\\bf{10^5\\cdot cm^{-3}}$', labelpad=-80, fontweight="bold")
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12, labelsize=16)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6, labelsize=14)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.set_yticks(range(200, 2001, 200))
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, labelsize=16)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5, labelsize=14)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(single_dt[0].strftime('%Y-%m-%d') + ' He+ altitude-time dynamics (single-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi)
plt.close()

fig, ax=plt.subplots(1, 1, figsize=(30*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of $\\bf O_2^+$ ions density\n{single_dt[0].strftime('%Y-%m-%d')}", fontweight="bold")
cp = plt.imshow(single_grid_N_O2, origin='lower', aspect='auto', cmap=my_cmap, vmin=0, vmax=1,
    extent=(single_dt[0], single_dt[-1], np.min(single_ylist2), np.max(single_ylist2)))
cbar = fig.colorbar(cp, pad=0.05) # Add a colorbar to a plot
cbar.set_label(label='$\\bf N_{O_2^+}$, $\\bf{10^5\\cdot cm^{-3}}$', labelpad=-65, fontweight="bold")
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12, labelsize=16)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6, labelsize=14)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, labelsize=16)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5, labelsize=14)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(single_dt[0].strftime('%Y-%m-%d') + ' O2+ altitude-time dynamics (single-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi)
plt.close()

fig, ax=plt.subplots(1, 1, figsize=(30*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of $\\bf NO^+$ ions density\n{single_dt[0].strftime('%Y-%m-%d')}", fontweight="bold")
cp = plt.imshow(single_grid_N_NO, origin='lower', aspect='auto', cmap=my_cmap, vmin=0, vmax=1.8,
    extent=(single_dt[0], single_dt[-1], np.min(single_ylist2), np.max(single_ylist2)))
cbar = fig.colorbar(cp, pad=0.05) # Add a colorbar to a plot
cbar.set_label(label='$\\bf N_{NO^+}$, $\\bf{10^5\\cdot cm^{-3}}$', labelpad=-63, fontweight="bold")
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12, labelsize=16)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6, labelsize=14)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, labelsize=16)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5, labelsize=14)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(single_dt[0].strftime('%Y-%m-%d') + ' NO+ altitude-time dynamics (single-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi)
plt.close()

fig, ax=plt.subplots(1, 1, figsize=(30*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of cluster ions density\n{single_dt[0].strftime('%Y-%m-%d')}", fontweight="bold")
cp = plt.imshow(single_grid_Clust, origin='lower', aspect='auto', cmap=my_cmap, vmin=0, vmax=1.8,
    extent=(single_dt[0], single_dt[-1], np.min(single_ylist), np.max(single_ylist)))
cbar = fig.colorbar(cp, pad=0.05) # Add a colorbar to a plot
cbar.set_label(label='$\\bf N_{Clust}$, $\\bf{10^5\\cdot cm^{-3}}$', labelpad=-63, fontweight="bold")
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12, labelsize=16)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6, labelsize=14)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.set_yticks(range(200, 2001, 200))
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, labelsize=16)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5, labelsize=14)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(single_dt[0].strftime('%Y-%m-%d') + ' Cluster ions altitude-time dynamics (single-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi)
plt.close()

fig, ax=plt.subplots(1, 1, figsize=(30*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of $\\bf N^+$ ions density\n{single_dt[0].strftime('%Y-%m-%d')}", fontweight="bold")
cp = plt.imshow(single_grid_N_N, origin='lower', aspect='auto', cmap=my_cmap, vmin=0, vmax=0.09,
    extent=(single_dt[0], single_dt[-1], np.min(single_ylist), np.max(single_ylist)))
cbar = fig.colorbar(cp, pad=0.05) # Add a colorbar to a plot
cbar.set_label(label='$\\bf N_{N^+}$, $\\bf{10^5\\cdot cm^{-3}}$', labelpad=-71, fontweight="bold")
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12, labelsize=16)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6, labelsize=14)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.set_yticks(range(200, 2001, 200))
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, labelsize=16)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5, labelsize=14)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(single_dt[0].strftime('%Y-%m-%d') + ' N+ ions altitude-time dynamics (single-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi)
plt.close()
print(f'The data created in single-threaded mode was plotted in {time.time() - start_time} seconds')


print('Plotting the data created in multi-threaded mode...')
start_time = time.time()

fig, ax=plt.subplots(1, 1, figsize=(30*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of electron density\n{multi_dt[0].strftime('%Y-%m-%d')}", fontweight="bold")
cp = plt.imshow(multi_grid_Ne, origin='lower', aspect='auto', cmap=my_cmap, vmin=0, vmax=7,
    extent=(multi_dt[0], multi_dt[-1], np.min(multi_ylist), np.max(multi_ylist)))
cbar = fig.colorbar(cp, pad=0.05) # Add a colorbar to a plot
cbar.set_label(label='$\\bf{N_e}$, $\\bf{10^5\\cdot cm^{-3}}$', labelpad=-48, fontweight="bold")
cbar.set_ticks(np.arange(0, 7.1, 1))
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12, labelsize=16)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6, labelsize=14)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.set_yticks(range(200, 2001, 200))
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, labelsize=16)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5, labelsize=14)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(multi_dt[0].strftime('%Y-%m-%d') + ' Ne altitude-time dynamics (multi-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi)
plt.close()

fig, ax=plt.subplots(1, 1, figsize=(30*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of neutral temperature\n{multi_dt[0].strftime('%Y-%m-%d')}", fontweight="bold")
cp = plt.imshow(multi_grid_Tn, origin='lower', aspect='auto', cmap=my_cmap, vmin=500, vmax=1000,
    extent=(multi_dt[0], multi_dt[-1], np.min(multi_ylist), np.max(multi_ylist)))
cbar = fig.colorbar(cp, pad=0.05) # Add a colorbar to a plot
cbar.set_label(label='$\\bf{T_n}$, K', labelpad=-73, fontweight="bold")
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12, labelsize=16)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6, labelsize=14)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.set_yticks(range(200, 2001, 200))
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, labelsize=16)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5, labelsize=14)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(multi_dt[0].strftime('%Y-%m-%d') + ' Tn altitude-time dynamics (multi-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi)
plt.close()

fig, ax=plt.subplots(1, 1, figsize=(30*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of ion temperature\n{multi_dt[0].strftime('%Y-%m-%d')}", fontweight="bold")
cp = plt.imshow(multi_grid_Ti, origin='lower', aspect='auto', cmap=my_cmap, vmin=500, vmax=4200,
    extent=(multi_dt[0], multi_dt[-1], np.min(multi_ylist), np.max(multi_ylist)))
cbar = fig.colorbar(cp, pad=0.05) # Add a colorbar to a plot
cbar.set_label(label='$\\bf{T_i}$, K', labelpad=-73, fontweight="bold")
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12, labelsize=16)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6, labelsize=14)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.set_yticks(range(200, 2001, 200))
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, labelsize=16)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5, labelsize=14)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(multi_dt[0].strftime('%Y-%m-%d') + ' Ti altitude-time dynamics (multi-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi)
plt.close()

fig, ax=plt.subplots(1, 1, figsize=(30*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of electron temperature\n{multi_dt[0].strftime('%Y-%m-%d')}", fontweight="bold")
cp = plt.imshow(multi_grid_Te, origin='lower', aspect='auto', cmap=my_cmap, vmin=500, vmax=4200,
    extent=(multi_dt[0], multi_dt[-1], np.min(multi_ylist), np.max(multi_ylist)))
cbar = fig.colorbar(cp, pad=0.05) # Add a colorbar to a plot
cbar.set_label(label='$\\bf{T_e}$, K', labelpad=-73, fontweight="bold")
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12, labelsize=16)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6, labelsize=14)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.set_yticks(range(200, 2001, 200))
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, labelsize=16)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5, labelsize=14)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(multi_dt[0].strftime('%Y-%m-%d') + ' Te altitude-time dynamics (multi-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi)
plt.close()

fig, ax=plt.subplots(1, 1, figsize=(30*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of $\\bf O^+$ ions density\n{multi_dt[0].strftime('%Y-%m-%d')}", fontweight="bold")
cp = plt.imshow(multi_grid_N_O, origin='lower', aspect='auto', cmap=my_cmap, vmin=0, vmax=7,
    extent=(multi_dt[0], multi_dt[-1], np.min(multi_ylist), np.max(multi_ylist)))
cbar = fig.colorbar(cp, pad=0.05) # Add a colorbar to a plot
cbar.set_label(label='$\\bf{N_{O^+}}$, $\\bf{10^5\\cdot cm^{-3}}$', labelpad=-48, fontweight="bold")
cbar.set_ticks(np.arange(0, 7.1, 1))
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12, labelsize=16)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6, labelsize=14)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.set_yticks(range(200, 2001, 200))
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, labelsize=16)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5, labelsize=14)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(multi_dt[0].strftime('%Y-%m-%d') + ' O+ altitude-time dynamics (multi-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi)
plt.close()

fig, ax=plt.subplots(1, 1, figsize=(30*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of $\\bf H^+$ ions density\n{multi_dt[0].strftime('%Y-%m-%d')}", fontweight="bold")
cp = plt.imshow(multi_grid_N_H, origin='lower', aspect='auto', cmap=my_cmap, vmin=0, vmax=0.07,
    extent=(multi_dt[0], multi_dt[-1], np.min(multi_ylist), np.max(multi_ylist)))
cbar = fig.colorbar(cp, pad=0.05) # Add a colorbar to a plot
cbar.set_label(label='$\\bf N_{H^+}$, $\\bf{10^5\\cdot cm^{-3}}$', labelpad=-70, fontweight="bold")
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12, labelsize=16)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6, labelsize=14)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.set_yticks(range(200, 2001, 200))
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, labelsize=16)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5, labelsize=14)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(multi_dt[0].strftime('%Y-%m-%d') + ' H+ altitude-time dynamics (multi-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi)
plt.close()

fig, ax=plt.subplots(1, 1, figsize=(30*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of $\\bf He^+$ ions density\n{multi_dt[0].strftime('%Y-%m-%d')}", fontweight="bold")
cp = plt.imshow(multi_grid_N_He, origin='lower', aspect='auto', cmap=my_cmap, vmin=0, vmax=0.008,
    extent=(multi_dt[0], multi_dt[-1], np.min(multi_ylist), np.max(multi_ylist)))
cbar = fig.colorbar(cp, pad=0.05) # Add a colorbar to a plot
cbar.set_label(label='$\\bf N_{He^+}$, $\\bf{10^5\\cdot cm^{-3}}$', labelpad=-80, fontweight="bold")
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12, labelsize=16)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6, labelsize=14)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.set_yticks(range(200, 2001, 200))
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, labelsize=16)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5, labelsize=14)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(multi_dt[0].strftime('%Y-%m-%d') + ' He+ altitude-time dynamics (multi-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi)
plt.close()

fig, ax=plt.subplots(1, 1, figsize=(30*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of $\\bf O_2^+$ ions density\n{multi_dt[0].strftime('%Y-%m-%d')}", fontweight="bold")
cp = plt.imshow(multi_grid_N_O2, origin='lower', aspect='auto', cmap=my_cmap, vmin=0, vmax=1,
    extent=(multi_dt[0], multi_dt[-1], np.min(multi_ylist2), np.max(multi_ylist2)))
cbar = fig.colorbar(cp, pad=0.05) # Add a colorbar to a plot
cbar.set_label(label='$\\bf N_{O_2^+}$, $\\bf{10^5\\cdot cm^{-3}}$', labelpad=-65, fontweight="bold")
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12, labelsize=16)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6, labelsize=14)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, labelsize=16)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5, labelsize=14)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(multi_dt[0].strftime('%Y-%m-%d') + ' O2+ altitude-time dynamics (multi-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi)
plt.close()

fig, ax=plt.subplots(1, 1, figsize=(30*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of $\\bf NO^+$ ions density\n{multi_dt[0].strftime('%Y-%m-%d')}", fontweight="bold")
cp = plt.imshow(multi_grid_N_NO, origin='lower', aspect='auto', cmap=my_cmap, vmin=0, vmax=1.8,
    extent=(multi_dt[0], multi_dt[-1], np.min(multi_ylist2), np.max(multi_ylist2)))
cbar = fig.colorbar(cp, pad=0.05) # Add a colorbar to a plot
cbar.set_label(label='$\\bf N_{NO^+}$, $\\bf{10^5\\cdot cm^{-3}}$', labelpad=-63, fontweight="bold")
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12, labelsize=16)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6, labelsize=14)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, labelsize=16)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5, labelsize=14)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(multi_dt[0].strftime('%Y-%m-%d') + ' NO+ altitude-time dynamics (multi-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi)
plt.close()

fig, ax=plt.subplots(1, 1, figsize=(30*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of cluster ions density\n{multi_dt[0].strftime('%Y-%m-%d')}", fontweight="bold")
cp = plt.imshow(multi_grid_Clust, origin='lower', aspect='auto', cmap=my_cmap, vmin=0, vmax=1.8,
    extent=(multi_dt[0], multi_dt[-1], np.min(multi_ylist), np.max(multi_ylist)))
cbar = fig.colorbar(cp, pad=0.05) # Add a colorbar to a plot
cbar.set_label(label='$\\bf N_{Clust}$, $\\bf{10^5\\cdot cm^{-3}}$', labelpad=-63, fontweight="bold")
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12, labelsize=16)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6, labelsize=14)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.set_yticks(range(200, 2001, 200))
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, labelsize=16)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5, labelsize=14)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(multi_dt[0].strftime('%Y-%m-%d') + ' Cluster ions altitude-time dynamics (multi-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi)
plt.close()

fig, ax=plt.subplots(1, 1, figsize=(30*cm, 10*cm), dpi=dpi)
ax.set_title(f"Altitude-time dynamics of $\\bf N^+$ ions density\n{multi_dt[0].strftime('%Y-%m-%d')}", fontweight="bold")
cp = plt.imshow(multi_grid_N_N, origin='lower', aspect='auto', cmap=my_cmap, vmin=0, vmax=0.09,
    extent=(multi_dt[0], multi_dt[-1], np.min(multi_ylist), np.max(multi_ylist)))
cbar = fig.colorbar(cp, pad=0.05) # Add a colorbar to a plot
cbar.set_label(label='$\\bf N_{N^+}$, $\\bf{10^5\\cdot cm^{-3}}$', labelpad=-71, fontweight="bold")
ax.xaxis.set_major_locator(HourLocator(byhour=range(0, 24, 2)))
ax.xaxis.set_major_formatter(DateFormatter('%H'))
ax.xaxis.set_minor_locator(HourLocator(byhour=range(1, 24, 2)))
ax.tick_params(axis="x", which="major", pad=5, direction = 'out', length=12, labelsize=16)
ax.tick_params(axis="x", which="minor", direction = 'out', length=6, labelsize=14)
ax.set_xlabel('Time, UT', fontweight="bold")
ax.set_yticks(range(200, 2001, 200))
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, labelsize=16)
ax.tick_params(axis="y", which="minor", direction = 'out', length=5, labelsize=14)
ax.set_ylabel('h, km', fontweight="bold")
plt.savefig(multi_dt[0].strftime('%Y-%m-%d') + ' N+ ions altitude-time dynamics (multi-threaded mode)' + '.png', bbox_inches='tight', dpi=dpi)
plt.close()
print(f'The data created in multi-threaded mode was plotted in {time.time() - start_time} seconds')
