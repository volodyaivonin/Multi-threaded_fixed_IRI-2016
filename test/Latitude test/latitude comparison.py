import re
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

output_dir = './'


IRI_WEB_filename = 'latitude_test_IRI_WEB_IRI-2016_280.000km_110deg_E_25.04.2023_3.00UT.dat'
iritest_filename = 'IRI-2016 latitude from iritest 280.000km_110deg_E_25.04.2023_3.00UT.7'

matplotlib.use('Agg')
cm = 1/2.54
plt.rcParams["font.family"] = "Times New Roman"
plt.rcParams["font.size"] = 14

iritest_lat_deg = []
iritest_Ne_cm3 = []
iritest_Tn_K = []
iritest_Ti_K = []
iritest_Te_K = []
iritest_O = []
iritest_N = []
iritest_H = []
iritest_He = []
iritest_O2 = []
iritest_NO = []
iritest_Clust = []
NAN = np.nan
with open(iritest_filename, 'r') as f:
    prev_line = ''
    table_origin = False
    sch = 0
    for line in f: #File reading
        if (not table_origin):
            if (prev_line == '-\n'):
                table_origin = True
        if line != '' and line != ' ' and table_origin:
            if sch >= 2:
                s0 = re.split('\s+|\*+|\n', line)[:-1]
                s1 = s0[1:] if s0[0]=='' else s0
                s2 = [x for x in s1 if x != '']
                iritest_lat_deg.append(float(s1[0]))
                Ne = float(s1[1])
                iritest_Ne_cm3.append(Ne/1.E5 if Ne >= 0 else 0.0)
                iritest_Tn_K.append(float(s1[3]) if float(s1[3]) >= 0 else 0.0)
                iritest_Ti_K.append(float(s1[4]) if float(s1[4]) >= 0 else 0.0)
                iritest_Te_K.append(float(s1[5]) if float(s1[5]) >= 0 else 0.0)
                if len(s2) == len(s1):
                    iritest_O.append(float(s1[6]) if float(s1[6]) >= 0 else 0.0)
                    iritest_N.append(float(s1[7]) if float(s1[7]) >= 0 else 0.0)
                    iritest_H.append(float(s1[8]) if float(s1[8]) >= 0 else 0.0)
                    iritest_He.append(float(s1[9]) if float(s1[9]) >= 0 else 0.0)
                    iritest_O2.append(float(s1[10]) if float(s1[10]) >= 0 else 0.0)
                    iritest_NO.append(float(s1[11]) if float(s1[11]) >= 0 else 0.0)
                    iritest_Clust.append(float(s1[12]) if float(s1[12]) >= 0 else 0.0)
                else:
                    iritest_O.append(NAN)
                    iritest_N.append(NAN)
                    iritest_H.append(float(s2[6]) if float(s2[6]) >= 0 else 0.0)
                    iritest_He.append(float(s2[7]) if float(s2[7]) >= 0 else 0.0)
                    iritest_O2.append(NAN)
                    iritest_NO.append(NAN)
                    iritest_Clust.append(float(s2[8]) if float(s2[8]) >= 0 else 0.0)
                    
            sch += 1
        prev_line = line

IRI_WEB_lat_deg = []
IRI_WEB_Ne_cm3 = []
IRI_WEB_Tn_K = []
IRI_WEB_Ti_K = []
IRI_WEB_Te_K = []
IRI_WEB_N_O = []
IRI_WEB_N_N = []
IRI_WEB_N_H = []
IRI_WEB_N_He = []
IRI_WEB_N_O2 = []
IRI_WEB_N_NO = []
IRI_WEB_N_Clust = []
with open(IRI_WEB_filename, 'r') as f:
     for i, line in enumerate(f): #File reading
        if line != '\n':
            if i != 0:
                s1 = line.split('\t')
                IRI_WEB_lat_deg.append(float(s1[0]))
                IRI_WEB_Ne_cm3.append(float(s1[1]))
                IRI_WEB_Tn_K.append(float(s1[2]))
                IRI_WEB_Ti_K.append(float(s1[3]))
                IRI_WEB_Te_K.append(float(s1[4]))
                IRI_WEB_N_O.append(float(s1[5]))
                IRI_WEB_N_N.append(float(s1[6]))
                IRI_WEB_N_H.append(float(s1[7]))
                IRI_WEB_N_He.append(float(s1[8]))
                IRI_WEB_N_O2.append(float(s1[9]))
                IRI_WEB_N_NO.append(float(s1[10]))
                IRI_WEB_N_Clust.append(float(s1[11]))

fig, ax = plt.subplots(1, 1, figsize=(30*cm, 12*cm))
fig.tight_layout()
plt.suptitle(t = 'Comparison 25.04.2023 03:00 UT 110$\\degree$ E', fontsize=17, fontweight='bold', y=1.02)
# ax.plot(IRI_WEB_lat_deg, IRI_WEB_N_O, lw=3, label='Modified lib version of IRI-2016 (subroutine "IRI_WEB")', ls='-')
# ax.plot(iritest_lat_deg, iritest_O, 'o--', lw=3, label='Version of IRI-2016 from $\\bf iritest.for$')
ax.plot(IRI_WEB_lat_deg, IRI_WEB_Ne_cm3, lw=3, label='Modified lib version of IRI-2016', ls='-')
ax.plot(iritest_lat_deg, iritest_Ne_cm3, '--', lw=3, label='Version of IRI-2016 from $\\bf iritest.for$')
ax.legend(loc='upper right', prop={'size': 14})
ax.set_xlim(-100, 100)
ax.set_xticks(np.arange(-90, 91, 10))
ax.set_ylabel('$\\bf N_e$, $\\bf 10^5 \\cdot cm^{-3}$', fontweight='bold')
ax.set_xlabel('Latitude, $\\bf \\degree$', fontweight='bold')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.grid()
plt.savefig('Comparison Ne 25.04.2023 03.00 UT 110 E.png', bbox_inches='tight', dpi=600)
plt.close()

fig, ax = plt.subplots(1, 1, figsize=(30*cm, 12*cm))
fig.tight_layout()
plt.suptitle(t = 'Comparison 25.04.2023 03:00 UT 110$\\degree$ E', fontsize=17, fontweight='bold', y=1.02)
ax.plot(iritest_lat_deg, iritest_Tn_K, '--', lw=3, label='$T_n$ (Version of IRI-2016 from $\\bf iritest.for$)')
ax.plot(iritest_lat_deg, iritest_Ti_K, '--', lw=3, label='$T_i$ (Version of IRI-2016 from $\\bf iritest.for$)')
ax.plot(iritest_lat_deg, iritest_Te_K, '-', lw=3, label='$T_e$ (Version of IRI-2016 from $\\bf iritest.for$)')
ax.plot(IRI_WEB_lat_deg, IRI_WEB_Tn_K, ':', lw=3, label='$T_n$ (Modified lib version of IRI-2016)')
ax.plot(IRI_WEB_lat_deg, IRI_WEB_Ti_K, ':', lw=3, label='$T_i$ (Modified lib version of IRI-2016)')
ax.plot(IRI_WEB_lat_deg, IRI_WEB_Te_K, '--', c='k', lw=3, label='$T_e$ (Modified lib version of IRI-2016)')
ax.legend(loc='center right', prop={'size': 14}, ncol=2)
ax.set_xlim(-100, 100)
ax.set_xticks(np.arange(-90, 91, 10))
ax.set_ylabel('T, K', fontweight='bold')
ax.set_xlabel('Latitude, $\\bf \\degree$', fontweight='bold')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.grid()
plt.savefig('Comparison T 25.04.2023 03.00 UT 110 E.png', bbox_inches='tight', dpi=600)
plt.close()

plt.rcParams["font.size"] = 40
legend_font_size = 40
fig_size = (30*cm, 18*cm)

marker_size = 14
line_width = 6
fig, ax = plt.subplots(1, 1, figsize=fig_size)
fig.tight_layout()
plt.suptitle(t = '$\\bf iritest.for$ on 25.04.2023 03:00 UT 110$\\degree$ E', fontsize=17, fontweight='bold', y=1.02)
ax.plot(iritest_lat_deg, iritest_O, 'o-', markersize=marker_size, lw=line_width, label='$N_{O^+}$')
ax.plot(iritest_lat_deg, iritest_N, 'o-', markersize=marker_size, lw=line_width, label='$N_{N^+}$')
ax.plot(iritest_lat_deg, iritest_O2, 'o-', markersize=marker_size, lw=line_width, label='$N_{O_2^+}$')
ax.plot(iritest_lat_deg, iritest_NO, 'o-', markersize=marker_size, lw=line_width, label='$N_{NO^+}$')
ax.plot(iritest_lat_deg, iritest_H, '-', lw=line_width, label='$N_{H^+}$')
ax.plot(iritest_lat_deg, iritest_He, '--', c='y', lw=line_width, label='$N_{He^+}$')
ax.plot(iritest_lat_deg, iritest_Clust, ':', c='k', lw=line_width, label='Cluster ions')
legend = ax.legend(loc='center', prop={'size': legend_font_size}, ncol=2, frameon=False, bbox_to_anchor=(0.5, 0.475))
legend.get_frame().set_facecolor((0, 0, 0, 0))
ax.set_xlim(-100, 100)
ax.set_xticks(np.arange(-90, 91, 20))
ax.set_ylabel('$\\bf N_i$, %*10', fontweight='bold')
ax.set_yticks(range(0, 1001, 200))
# ax.tick_params(axis="y", which="major", pad=5, direction = 'out', length=10, width=2)
ax.set_ylim(-50, 1000)
ax.set_xlabel('Latitude, $\\bf \\degree$', fontweight='bold')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.grid()
plt.savefig('Ni from iritest 25.04.2023 03.00 UT 110 E.png', bbox_inches='tight', dpi=600)
plt.close()

fig, ax = plt.subplots(1, 1, figsize=fig_size)
fig.tight_layout()
plt.suptitle(t = 'Modified lib version of IRI-2016 on 25.04.2023 03:00 UT 110$\\degree$ E', fontsize=17,
    fontweight='bold', y=1.02)
ax.plot(IRI_WEB_lat_deg, IRI_WEB_N_O, '-', lw=line_width, label='$N_{O^+}$')
ax.plot(IRI_WEB_lat_deg, IRI_WEB_N_N, '-', lw=line_width, label='$N_{N^+}$')
ax.plot(IRI_WEB_lat_deg, IRI_WEB_N_O2, '-', lw=line_width, label='$N_{O_2^+}$')
ax.plot(IRI_WEB_lat_deg, IRI_WEB_N_NO, '-', lw=line_width, label='$N_{NO^+}$')
ax.plot(IRI_WEB_lat_deg, IRI_WEB_N_H, '-', lw=line_width, label='$N_{H^+}$')
ax.plot(IRI_WEB_lat_deg, IRI_WEB_N_He, '--', c='y', lw=line_width, label='$N_{He^+}$')
ax.plot(IRI_WEB_lat_deg, IRI_WEB_N_Clust, ':', c='k', lw=line_width, label='Cluster ions')
legend = ax.legend(loc='center', prop={'size': legend_font_size}, ncol=2, frameon=False, bbox_to_anchor=(0.5, 0.475))
legend.get_frame().set_facecolor((0, 0, 0, 0))
ax.set_xlim(-100, 100)
ax.set_xticks(np.arange(-90, 91, 20))
ax.set_ylabel('$\\bf N_i$, %*10', fontweight='bold')
ax.set_xlabel('Latitude, $\\bf \\degree$', fontweight='bold')
ax.set_yticks(range(0, 1001, 200))
ax.set_ylim(-50, 1000)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.grid()
plt.savefig('Ni from modified lib version of IRI-2016 25.04.2023 03.00 UT 110 E.png', bbox_inches='tight', dpi=600)
plt.close()